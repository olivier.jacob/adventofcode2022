package AdventOfCode2022.day9;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class RopeProblem {

    private List<Move> moves;
    private static final int NB_KNOTS = 10;
    private static Rope rope = Rope.withKnots(NB_KNOTS);

    public static void main(String[] args) throws IOException {
        var observer = new VisitedPositionsObserver();
        rope.getknot(NB_KNOTS - 1).observe(observer);

        var problem = new RopeProblem();
        problem.loadMoves("day9/input.txt");
        problem.applyMoves();

        System.out.println("Tail visited " + observer.getVisitedPositions().size() + " positions");
    }

    private void applyMoves() {
        for (Move move : moves) {
            rope.apply(move);
        }
    }

    private void loadMoves(String filename) throws IOException {
        var path = Path.of(this.getClass().getClassLoader().getResource(filename).getPath());
        this.moves = Files.readAllLines(path).stream()
            .map(this::toMove)
            .toList();
    }

    private Move toMove(String s) {
        return new Move(
            s.substring(0, 1),
            Integer.parseInt(s.substring(2))
        );
    }
}
