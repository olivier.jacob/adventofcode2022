package AdventOfCode2022.day9;

import java.util.HashSet;

public class VisitedPositionsObserver implements KnotObserver {
    private final HashSet<Position> visitedPositions;

    public VisitedPositionsObserver() {
        visitedPositions = new HashSet<>();
        visitedPositions.add(new Position(0, 0));
    }

    @Override
    public void knotMoved(Position from, Position to) {
        visitedPositions.add(to);
    }

    public HashSet<Position> getVisitedPositions() {
        return visitedPositions;
    }
}
