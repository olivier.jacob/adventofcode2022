package AdventOfCode2022.day9;

public record Position(int x, int y) {
    public boolean isAdjacentTo(Position other) {
        return isHorizonatallyAdjacent(other) && isVerticallyAdjacent(other);
    }

    private boolean isHorizonatallyAdjacent(Position other) {
        return Math.abs(other.x - x) <= 1;
    }

    private boolean isVerticallyAdjacent(Position other) {
        return Math.abs(other.y - y) <= 1;
    }

    public Position right() {
        return new Position(x + 1, y);
    }

    public Position left() {
        return new Position(x - 1, y);
    }

    public Position up() {
        return new Position(x, y + 1);
    }

    public Position down() {
        return new Position(x, y - 1);
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
