package AdventOfCode2022.day9;

public record Move(String direction, int count) {

}
