package AdventOfCode2022.day9;

public interface KnotObserver {
    void knotMoved(Position from, Position to);
}
