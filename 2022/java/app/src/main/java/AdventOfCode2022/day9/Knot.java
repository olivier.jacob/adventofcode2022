package AdventOfCode2022.day9;

import java.util.Optional;

public class Knot {
    private Position position;
    private Optional<KnotObserver> observer;


    public Knot(Position position) {
        this.position = position;
        this.observer = Optional.empty();
    }

    public Knot(Position position, KnotObserver observer) {
        this.position = position;
        this.observer = Optional.of(observer);
    }

    public void move(String direction) {
        var originalPosition = this.position;
        this.position = switch (direction) {
            case "R" -> position.right();
            case "L" -> position.left();
            case "U" -> position.up();
            case "D" -> position.down();
            default -> throw new RuntimeException("Unknown direction " + direction);
        };

        observer.ifPresent(o -> o.knotMoved(originalPosition, this.position));
    }

    public void trackKnot(Knot other) {
        if (!this.position.isAdjacentTo(other.position)) {
            Position newPosition;
            Position initialPosition = this.position;

            if (this.position.x() == other.position.x()) {
                var signum = (int) Math.signum((float) other.position.y() - this.position.y());
                newPosition = new Position(this.position.x(), this.position.y() + signum);
            } else if (this.position.y() == other.position.y()) {
                var signum = (int) Math.signum((float) other.position.x() - this.position.x());
                newPosition = new Position(this.position.x() + signum, this.position.y());
            } else {
                var signumX = (int) Math.signum((float) other.position.x() - this.position.x());
                var signumY = (int) Math.signum((float) other.position.y() - this.position.y());

                newPosition = new Position(this.position.x() + signumX, this.position.y() + signumY  );
            }

            observer.ifPresent(o -> o.knotMoved(initialPosition, newPosition));

            this.position = newPosition;
        }
    }

    public Position position() {
        return position;
    }

    public void observe(KnotObserver observer) {
        this.observer = Optional.of(observer);
    }
}
