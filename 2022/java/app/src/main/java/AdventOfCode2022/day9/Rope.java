package AdventOfCode2022.day9;

import java.util.List;
import java.util.stream.IntStream;

public record Rope(List<Knot> knots) {
    public static Rope withKnots(int count) {
        var knots = IntStream.range(0, count).mapToObj(c -> new Knot(new Position(0, 0)))
            .toList();

        return new Rope(knots);
    }

    public void apply(Move move) {
        Knot head = knots.get(0);

        for (int i = 0; i < move.count(); i++) {
            head.move(move.direction());

            for (int k = 1; k < knots.size(); k++) {
                knots.get(k).trackKnot(knots.get(k - 1));
            }
        }
    }

    public Knot getknot(int index) {
        return knots.get(index);
    }
}
