package AdventOfCode2022.day12;

public class HeightMap {
    private final int[][] heights;
    private final Cell startCell;
    private final Cell endCell;
    private final int width;
    private final int height;

    public HeightMap(int[][] heights, Cell startCell, Cell endCell) {
        this.heights = heights;
        this.startCell = startCell;
        this.endCell = endCell;
        this.height = heights.length;
        this.width = heights[0].length;
    }

    public Cell getStartCell() {
        return startCell;
    }

    public Cell getEndCell() {
        return endCell;
    }

    public int getHeightAt(int row, int col) {
        return heights[row][col];
    }

    public int[][] getHeights() {
        return heights;
    }
}
