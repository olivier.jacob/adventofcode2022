package AdventOfCode2022.day12;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class PathFinder {

    public List<Cell> shortestPath(HeightMap heightMap) {
        int[][] heights = heightMap.getHeights();

        return shortestPath(
            heights,
            heightMap.getStartCell(),
            current -> current.equals(heightMap.getEndCell()),
            (move, current) -> heights[move.destRow][move.destCol] - heights[current.row()][current.col()] <= 1
        );
    }

    public List<Cell> findMostScenicPath(HeightMap heightMap) {
        int[][] heights = heightMap.getHeights();

        return shortestPath(
            heights,
            heightMap.getEndCell(),
            current -> heights[current.row()][current.col()] == 0,
            (move, current) -> heights[move.destRow][move.destCol] - heights[current.row()][current.col()] >= -1
        );
    }

    private List<Cell> shortestPath(int[][] heights, Cell start, Predicate<Cell> isGoalReached, BiPredicate<Move, Cell> eligibleMove) {
        Map<Cell, Cell> cameFrom = new HashMap<>();

        Map<Cell, Integer> fScore = new HashMap<>();
        Map<Cell, Integer> gScore = new HashMap<>();

        PriorityQueue<Cell> openSet = new PriorityQueue<>(
            Comparator.comparingInt(c -> fScore.getOrDefault(c, Integer.MAX_VALUE))
        );

        openSet.add(start);
        fScore.put(start, heuristic(start));
        gScore.put(start, 0);

        while (!openSet.isEmpty()) {
            var current = openSet.peek();
            if (isGoalReached.test(current)) {
                return reconstructPath(cameFrom, current);
            }

            openSet.remove(current);

            for (Move m : findNextMoves(heights, current)) {
                if (!eligibleMove.test(m, current))
                    continue;

                var neighboor = m.getDestCell();
                var tentativeGScore = gScore.getOrDefault(current, Integer.MAX_VALUE) + 1;
                if (tentativeGScore < gScore.getOrDefault(neighboor, Integer.MAX_VALUE)) {
                    cameFrom.put(neighboor, current);
                    gScore.put(neighboor, tentativeGScore);
                    fScore.put(neighboor, tentativeGScore + heuristic(neighboor));

                    if (!openSet.contains(neighboor)) {
                        openSet.add(neighboor);
                    }
                }
            }
        }

        return List.of();
    }

    private List<Cell> reconstructPath(Map<Cell, Cell> cameFrom, Cell current) {
        LinkedList<Cell> path = new LinkedList<>();

        path.addFirst(current);
        while (cameFrom.containsKey(current)) {
            current = cameFrom.get(current);
            path.addFirst(current);
        }

        return path;
    }

    private Integer heuristic(Cell cell) {
        // Dijkstra version
        return 0;
    }

    private List<Move> findNextMoves(int[][] heights, Cell currentCell) {
        int row = currentCell.row();
        int col = currentCell.col();

        int gridHeight = heights.length;
        int gridWidth = heights[0].length;

        List<Move> nextMoves = new ArrayList<>();

        if (row + 1 < gridHeight) {
            nextMoves.add(new Move(row + 1, col));
        }
        if (row - 1 >= 0) {
            nextMoves.add(new Move(row - 1, col));
        }
        if (col + 1 < gridWidth) {
            nextMoves.add(new Move(row, col + 1));
        }
        if (col - 1 >= 0) {
            nextMoves.add(new Move(row, col - 1));
        }

        return nextMoves;
    }

    private record Move(int destRow, int destCol) {
        public Cell getDestCell() {
            return new Cell(destRow, destCol);
        }
    }
}
