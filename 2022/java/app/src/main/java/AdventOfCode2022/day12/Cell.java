package AdventOfCode2022.day12;

public record Cell(int row, int col) {
}
