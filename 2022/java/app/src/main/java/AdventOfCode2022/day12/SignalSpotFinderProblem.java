package AdventOfCode2022.day12;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class SignalSpotFinderProblem {

    public static void main(String[] args) throws IOException {
        var problem = new SignalSpotFinderProblem();
        var heightMap = problem.parseInput("day12/input.txt");

        var shortestPath = problem.shortestPath(heightMap);
        System.out.println("Shortest path has steps " + (shortestPath.size() - 1));

        var mostScenicPath = problem.mostScenicPath(heightMap);
        System.out.println("Most scenic path has steps " + (mostScenicPath.size() - 1));
    }

    public List<Cell> mostScenicPath(HeightMap heightMap) {
        PathFinder pathFinder = new PathFinder();
        return pathFinder.findMostScenicPath(heightMap);
    }

    private HeightMap parseInput(String filename) throws IOException {
        return new InputParser().parse(filename);
    }

    public HeightMap parse(List<String> exampleInput) {
        return new InputParser().parse(exampleInput);
    }

    public List<Cell> shortestPath(HeightMap heightMap) {
        PathFinder pathFinder = new PathFinder();

        return pathFinder.shortestPath(heightMap);
    }

    public static class InputParser {

        public HeightMap parse(String filename) throws IOException {
            var path = Path.of(this.getClass().getClassLoader().getResource(filename).getPath());
            var lines = Files.readAllLines(path);

            return parse(lines);
        }

        public HeightMap parse(List<String>lines) {
            int[][] heights = new int[lines.size()][lines.get(0).length()];
            Cell startCell = null;
            Cell endCell = null;

            for (int row = 0; row < lines.size(); row++) {
                var chars = lines.get(row).chars().toArray();
                for (int col = 0; col < chars.length; col++) {
                    if (chars[col] == 'S') {
                        startCell = new Cell(row, col);
                        heights[row][col] = 0;
                    } else if (chars[col] == 'E') {
                        endCell = new Cell(row, col);
                        heights[row][col] = 'z' - 'a';
                    } else {
                        heights[row][col] = chars[col] - 'a';
                    }
                }
            }

            return new HeightMap(heights, startCell, endCell);
        }
    }
}
