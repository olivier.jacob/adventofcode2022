package AdventOfCode2022.day3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class PriorityPackingProblem {

    public static void main(String[] args) throws IOException {
        var priorityPackingProblem = new PriorityPackingProblem();
        var rucksacks = priorityPackingProblem.loadRucksacks("day3/input.txt");
        var prioritiesSum = priorityPackingProblem.scoreAllRucksacks(rucksacks);
        System.out.println("Sum of priorities is " + prioritiesSum);

        prioritiesSum = priorityPackingProblem.scoreRucksacksByGroupsOf(rucksacks, 3);
        System.out.println("Sum of priorities for groups is " + prioritiesSum);
    }

    private List<String> loadRucksacks(String filename) throws IOException {
        var path = Path.of(this.getClass().getClassLoader().getResource(filename).getPath());
        return Files.readAllLines(path);
    }

    public int scoreAllRucksacks(List<String> rucksacks) {
        return rucksacks.stream()
            .mapToInt(this::commonItemPriority)
            .sum();
    }

    public int scoreRucksacksByGroupsOf(List<String> rucksacks, int groupSize) {
        var elvesGroups = makeGroups(rucksacks, groupSize);

        return elvesGroups.stream()
            .map(this::commonInAll)
            .mapToInt(this::itemPriority)
            .sum();
    }

    private List<List<String>> makeGroups(List<String> rucksacks, int groupSize) {
        List<List<String>> groups = new ArrayList<>();

        List<String> currentGroup = new ArrayList<>();
        for (int i = 0; i < rucksacks.size(); i++) {
            if (i != 0 && i % groupSize == 0) {
                groups.add(currentGroup);
                currentGroup = new ArrayList<>();
            }

            currentGroup.add(rucksacks.get(i));
        }
        groups.add(currentGroup);

        return groups;
    }

    public char findCommonItem(String rucksack) {
        var middle = rucksack.length() / 2;

        var compartement1 = rucksack.substring(0, middle);
        var compartement2 = rucksack.substring(middle);

        return commonInAll(List.of(compartement1, compartement2));

//        Arrays.sort(compartement1);
//        Arrays.sort(compartement2);
//
//        for (int i = 0, j = 0; i < compartement1.length && j < compartement2.length ;) {
//            if (compartement1[i] == compartement2[j]) {
//                return compartement1[i];
//            } else if (compartement1[i] < compartement2[j]) {
//                while (i < compartement1.length && compartement1[i] < compartement2[j]) i++;
//            } else if (compartement1[i] > compartement2[j]) {
//                while (j < compartement2.length && compartement1[i] > compartement2[j]) j++;
//            } else {
//                return '0';
//            }
//        }
//
//        return '0';
    }

    public int itemPriority(char item) {
        if (item == '0') {
            return 0;
        } else if (item >= 'a') {
            return 1 + (item - 'a');
        } else {
            return 27 + (item - 'A');
        }
    }

    public int commonItemPriority(String rucksack) {
        var itemPriority = itemPriority(findCommonItem(rucksack));

        System.out.println(String.format("Rucksack <%s> priority is <%d>", rucksack, itemPriority));
        return itemPriority;
    }

    public char commonInAll(List<String> rucksacks) {
        var firstRucksack = rucksacks.get(0);
        var otherRucksacks = rucksacks.subList(1, rucksacks.size());

        for (char item : firstRucksack.toCharArray()) {
            if (otherRucksacks.stream().allMatch(rucksack -> rucksack.contains(String.valueOf(item)))) {
                return item;
            }
        }

        return '0';
    }
}
