package AdventOfCode2022.day14.part1;

public class SandCave {
    public static final char EMPTY = '.';
    public static final char WALL = '#';
    public static final char SAND = 'o';
    public static final char SAND_HOLE = '+';

    private final char[][] cave;
    private final int originX;
    private final int originY;

    public SandCave(char[][] cave, int originX, int originY) {
        this.cave = cave;
        this.originX = originX;
        this.originY = originY;
    }

    public char[][] getCave() {
        return cave;
    }

    public int getOriginX() {
        return originX;
    }

    public int getOriginY() {
        return originY;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int y = 0; y < cave[0].length; y++) {
            for (int x = 0; x < cave.length; x++) {
                sb.append(cave[x][y]);
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    public boolean produceSandUnit() {
        return Sand.fallsFrom(this.getCave(), 500 - originX, 0);
    }
}
