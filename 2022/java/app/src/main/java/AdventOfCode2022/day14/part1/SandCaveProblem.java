package AdventOfCode2022.day14.part1;

import java.io.IOException;

public class SandCaveProblem {
    public static void main(String[] args) throws IOException {
        var parser = new SandCaveParser();
        var cave = parser.parse("day14/input.txt");

        var problem = new SandCaveProblem(cave);

        System.out.println("Overflows after sand units " + problem.countSandUnitsBeforeOverflow());
    }

    private SandCave cave;

    public SandCaveProblem(SandCave cave) {
        this.cave = cave;
    }

    public int countSandUnitsBeforeOverflow() {
        int count = 0;
        while (cave.produceSandUnit()) count++;
        return count;
    }
}
