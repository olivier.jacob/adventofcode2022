package AdventOfCode2022.day14.part2;

import AdventOfCode2022.day14.part2.SandCave.CaveCell;

import java.io.IOException;

public class SandCaveProblemPart2 {
    public static void main(String[] args) throws IOException {
        var parser = new SandCaveParser();
        var cave = parser.parse("day14/input.txt");

        var problem = new SandCaveProblemPart2(cave);

        System.out.println("Overflows after sand units " + problem.countSandUnitsBeforeFull());
    }

    private SandCave cave;

    public SandCaveProblemPart2(SandCave cave) {
        this.cave = cave;
    }

    public int countSandUnitsBeforeFull() {
        int count = 1;
        while (!cave.produceSandUnit().equals(new CaveCell(500 - this.cave.getOriginX(), 0))) count++;
        return count;
    }
}
