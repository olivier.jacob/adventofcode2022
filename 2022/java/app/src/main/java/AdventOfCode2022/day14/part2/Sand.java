package AdventOfCode2022.day14.part2;

import AdventOfCode2022.day14.part2.SandCave.CaveCell;

public class Sand {

    public static CaveCell fallsFrom(SandCave sandCave, int fromX, int fromY) {
        int x = fromX;
        int y = fromY;

        var cave = sandCave.getCave();

        if (x <= 0) {
            sandCave.expandLeft();
            return fallsFrom(sandCave, fromX + 1, fromY);
        }
        if (x + 1 == cave.size()) {
            sandCave.expandRight();
            return fallsFrom(sandCave, fromX, fromY);
        }

        if (sandCave.isEmpty(x, y + 1)) {
            return fallsFrom(sandCave, x, y + 1);
        } else if (sandCave.isEmpty(x - 1, y + 1)) {
            return fallsFrom(sandCave, x - 1, y + 1);
        } else if (x + 1 < cave.size() && sandCave.isEmpty(x + 1, y + 1)) {
            return fallsFrom(sandCave, x + 1, y + 1);
        } else {
            cave.get(x)[y] = SandCave.SAND;
            return new CaveCell(x, y);
        }
    }
}
