package AdventOfCode2022.day14.part1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

public class SandCaveParser {
    private static final String COORDINATES_SEPARATOR = " -> ";

    public SandCave parse(String filename) throws IOException {
        var path = Path.of(this.getClass().getClassLoader().getResource(filename).getPath());
        var lines = Files.readAllLines(path);

        return parse(lines);
    }

    public SandCave parse(List<String> lines) {
        var cave = createEmptyCave(lines);
        createCave(cave, lines);
        placeSandHole(cave);
        return cave;
    }

    private SandCave createEmptyCave(List<String> lines) {
        int minX = Integer.MAX_VALUE, maxX = 0, maxY = 0;

        for (String line : lines) {
            for (String coordinate : line.split(COORDINATES_SEPARATOR)) {
                var coordPair = coordinate.split(",");
                int x = Integer.parseInt(coordPair[0]);
                int y = Integer.parseInt(coordPair[1]);

                minX = Math.min(minX, x);
                maxX = Math.max(maxX, x);
                maxY = Math.max(maxY, y);
            }
        }

        var emptyCaveMap = new char[maxX - minX + 1][maxY + 1];
        for (char[] cols : emptyCaveMap) {
            Arrays.fill(cols, SandCave.EMPTY);
        }

        return new SandCave(emptyCaveMap, minX, 0);
    }

    private void createCave(SandCave map, List<String> lines) {
        var cave = map.getCave();
        for (String line : lines) {
            int lastX = -1, lastY = -1;
            for (String coordinate : line.split(COORDINATES_SEPARATOR)) {
                var coordPair = coordinate.split(",");
                int x = Integer.parseInt(coordPair[0]);
                int y = Integer.parseInt(coordPair[1]);

                cave[x - map.getOriginX()][y - map.getOriginY()] = SandCave.WALL;

                if (lastX != -1 && lastY != -1) {
                    drawWall(map, x, lastX, y, lastY);
                }

                lastX = x;
                lastY = y;
            }
        }
    }

    private void drawWall(SandCave map, int fromX, int toX, int fromY, int toY) {
        var cave = map.getCave();
        if (fromX == toX) {
            int startY = Math.min(fromY, toY);
            int endY = Math.max(fromY, toY);

            for (int y = startY; y <= endY; y++) {
                cave[fromX - map.getOriginX()][y - map.getOriginY()] = SandCave.WALL;
            }
        } else {
            int startX = Math.min(fromX, toX);
            int endX = Math.max(fromX, toX);
            // draw line
            for (int x = startX; x <= endX; x++) {
                cave[x - map.getOriginX()][fromY - map.getOriginY()] = SandCave.WALL;
            }
        }
    }

    private void placeSandHole(SandCave cave) {
        var cells = cave.getCave();
        cells[500 - cave.getOriginX()][0] = SandCave.SAND_HOLE;
    }
}
