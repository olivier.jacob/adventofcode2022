package AdventOfCode2022.day14.part1;

public class Sand {

    public static boolean fallsFrom(char[][] cave, int fromX, int fromY) {
        int x = fromX;
        int y = fromY;

        if (x <= 0 || x >= cave.length || y >= cave[0].length) {
            return false;
        }

        if (isEmpty(cave, x, y + 1)) {
            return fallsFrom(cave, x, y + 1);
        } else if (isEmpty(cave, x - 1, y + 1)) {
            return fallsFrom(cave, x - 1, y + 1);
        } else if (isEmpty(cave, x + 1, y + 1)) {
            return fallsFrom(cave, x + 1, y + 1);
        } else {
            cave[x][y] = SandCave.SAND;
        }

        return true;
    }

    private static boolean isEmpty(char[][] cave, int x, int y) {
        return cave[x][y] == '.';
    }
}
