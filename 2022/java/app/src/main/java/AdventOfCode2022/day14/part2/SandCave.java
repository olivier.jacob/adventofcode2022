package AdventOfCode2022.day14.part2;

import java.util.Arrays;
import java.util.LinkedList;

public class SandCave {
    public static final char EMPTY = '.';
    public static final char WALL = '#';
    public static final char SAND = 'o';
    public static final char SAND_HOLE = '+';

    private final LinkedList<char[]> cave;
    private int originX;
    private final int originY;
    private final int floorLevel;

    public SandCave(LinkedList<char[]> cave, int originX, int originY, int floorLevel) {
        this.cave = cave;
        this.originX = originX;
        this.originY = originY;
        this.floorLevel = floorLevel;
    }

    public LinkedList<char[]> getCave() {
        return cave;
    }

    public int getOriginX() {
        return originX;
    }

    public int getOriginY() {
        return originY;
    }

    public int getFloorLevel() {
        return floorLevel;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int y = 0; y < cave.getFirst().length; y++) {
            for (int x = 0; x < cave.size(); x++) {
                sb.append(cave.get(x)[y]);
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    public boolean isEmpty(int x, int y) {
        return cave.get(x)[y] == SandCave.EMPTY || cave.get(x)[y] == SandCave.SAND_HOLE;
    }

    public CaveCell produceSandUnit() {
        return Sand.fallsFrom(this, 500 - originX, 0);
    }

    public void expandLeft() {
        this.originX = this.originX - 1;
        var newColumn = new char[floorLevel + 1];
        Arrays.fill(newColumn, SandCave.EMPTY);
        newColumn[floorLevel] = WALL;

        cave.addFirst(newColumn);
    }

    public void expandRight() {
        var newColumn = new char[floorLevel + 1];
        Arrays.fill(newColumn, SandCave.EMPTY);
        newColumn[floorLevel] = WALL;

        cave.addLast(newColumn);
    }

    public record CaveCell(int x, int y) {}
}
