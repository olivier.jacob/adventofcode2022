package AdventOfCode2022.day10;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class ScreenDeviceProblem {
    private CPU cpu;
    private SignalStrenghObserver signalStrengthObserver;

    public static void main(String[] args) throws IOException {
        var problem = new ScreenDeviceProblem();
        problem.loadInstructions("day10/input.txt");
        problem.execute();
        var signalStrengthsSum = problem.sumSignalStrenghts();

        System.out.println("Sum is " + signalStrengthsSum);
    }


    private void loadInstructions(String filename) throws IOException {
        var path = Path.of(this.getClass().getClassLoader().getResource(filename).getPath());
        var lines = Files.readAllLines(path);

        List<Instruction> instructions = new InstructionParser().parse(lines);
        this.cpu = new CPU(instructions);
        this.signalStrengthObserver = new SignalStrenghObserver();
        cpu.registerObserver(this.signalStrengthObserver);
        cpu.registerObserver(new CRT());
    }

    private void execute() {
        while (cpu.hasNextInstruction()) {
            cpu.tick();
        }
    }

    private long sumSignalStrenghts() {
        return signalStrengthObserver.totalStrenghts();
    }
}
