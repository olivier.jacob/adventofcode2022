package AdventOfCode2022.day10;

import java.util.List;

public class InstructionParser {
    public List<Instruction> parse(List<String> input) {
        return input.stream()
            .map(InstructionParser::parseInstruction)
            .toList();
    }

    private static Instruction parseInstruction(String line) {
        if (line.startsWith("noop")) {
            return new Noop();
        } else if (line.startsWith("addx")) {
            return new AddX(Integer.parseInt(line.split(" ")[1]));
        }

        throw new RuntimeException("Parse exception. Unknow instruction: " + line);
    }
}
