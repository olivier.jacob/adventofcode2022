package AdventOfCode2022.day10;

import java.util.ArrayList;
import java.util.List;

public class SignalStrenghObserver implements CycleObserver {
    private static List<Integer> pollAt = List.of(20, 60, 100, 140, 180, 220);
    private List<Integer> strengths = new ArrayList<>();

    @Override
    public void cycleStarted(CPU cpu, int cycle) {
        if (pollAt.contains(cycle)) {
            strengths.add(cycle * cpu.getRegisterX().get());
        }
    }

    @Override
    public void cycleEnded(CPU cpu, int cycle) {
        // noop
    }

    public long totalStrenghts() {
        return strengths.stream().reduce(0, Integer::sum);
    }
}
