package AdventOfCode2022.day10;

import AdventOfCode2022.day10.CPU.Register;

import java.util.Objects;

public class AddX implements Instruction {
    private final int val;
    private int remainingCycles = 2;

    public AddX(int val) {
        this.val = val;
    }

    @Override
    public void doCycle(Register register) {
        remainingCycles--;
        if (isFinished()) {
            register.set(register.get() + this.val);
        }
    }

    @Override
    public boolean isFinished() {
        return remainingCycles == 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddX addX = (AddX) o;
        return val == addX.val;
    }

    @Override
    public int hashCode() {
        return Objects.hash(val);
    }
}
