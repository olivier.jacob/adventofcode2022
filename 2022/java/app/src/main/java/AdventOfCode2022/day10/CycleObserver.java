package AdventOfCode2022.day10;

public interface CycleObserver {
    void cycleStarted(CPU cpu, int cycle);
    void cycleEnded(CPU cpu, int cycle);
}
