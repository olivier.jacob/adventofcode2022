package AdventOfCode2022.day10;

import java.util.ArrayList;
import java.util.List;

public class CPU {
    private int cycle = 1;
    private Register x = new Register();

    private int currentInstruction = 0;
    private List<Instruction> instructions;
    private List<CycleObserver> cycleObservers;

    public CPU(List<Instruction> instructions) {
        this.instructions = instructions;
        this.cycleObservers = new ArrayList<>();
    }

    public boolean hasNextInstruction() {
        return currentInstruction < instructions.size();
    }

    public void tick() {
        var instr = instructions.get(currentInstruction);

        cycleStarted();

        instr.doCycle(x);
        if (instr.isFinished()) {
            currentInstruction++;
        }
        cycle++;

        cycleEnded();
    }

    private void cycleStarted() {
        cycleObservers.forEach(o -> o.cycleStarted(this, this.cycle));
    }

    private void cycleEnded() {
        cycleObservers.forEach(o -> o.cycleEnded(this, this.cycle));
    }

    public Register getRegisterX() {
        return x;
    }

    public void registerObserver(CycleObserver observer) {
        cycleObservers.add(observer);
    }

    public static class Register {
        private int value = 1;

        public int get() {
            return value;
        }

        public void set(int newValue) {
            this.value = newValue;
        }
    }
}
