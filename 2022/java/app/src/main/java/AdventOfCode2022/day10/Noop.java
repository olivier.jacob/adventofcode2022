package AdventOfCode2022.day10;

import AdventOfCode2022.day10.CPU.Register;

public record Noop() implements Instruction {
    @Override
    public void doCycle(Register register) {
        // noop
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
