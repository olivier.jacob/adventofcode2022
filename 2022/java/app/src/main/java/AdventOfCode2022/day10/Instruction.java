package AdventOfCode2022.day10;

import AdventOfCode2022.day10.CPU.Register;

public interface Instruction {
    void doCycle(Register register);

    boolean isFinished();
}
