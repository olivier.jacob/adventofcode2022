package AdventOfCode2022.day10;

public class CRT implements CycleObserver {
    private static final int ROW_LENGTH = 40;
    private int currentPos = 0;

    private int spritePos = 1;

    @Override
    public void cycleStarted(CPU cpu, int cycle) {
        if (isSprite()) {
            System.out.print("#");
        } else {
            System.out.print(".");
        }
        if (++currentPos % ROW_LENGTH == 0) {
            System.out.println("");
            currentPos = 0;
        }
    }

    @Override
    public void cycleEnded(CPU cpu, int cycle) {
        this.spritePos = cpu.getRegisterX().get();
    }

    private boolean isSprite() {
        return currentPos >= spritePos - 1 && currentPos <= spritePos + 1;
    }
}
