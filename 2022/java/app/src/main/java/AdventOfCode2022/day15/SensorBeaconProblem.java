package AdventOfCode2022.day15;

import javax.swing.text.Segment;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class SensorBeaconProblem {
    private final Set<Device> devices = new HashSet<>();
    private final Map<Long, List<Segment>> segmentsByLine = new HashMap<>();

    private final Optional<Long> targetLine;

    public SensorBeaconProblem() {
        this.targetLine = Optional.empty();
    }

    public SensorBeaconProblem(long targetLine) {
        this.targetLine = Optional.of(targetLine);
    }

    public void detectBeaconLessSpots(List<Sensor> sensors) {
        for (Sensor sensor : sensors) {
            drawSensorAndBeacon(sensor);
            drawSensingZone(sensor);
        }
    }

    private void drawSensorAndBeacon(Sensor sensor) {
        devices.add(new Device(sensor.x(), sensor.y()));
        devices.add(new Device(sensor.closestBeacon().x(), sensor.closestBeacon().y()));
    }

    public long countBeaconLessSpotsOnLine(long lineY) {
        var devicesOnLine = devices.stream().filter(d -> d.y() == lineY).toList();
        return segmentsByLine.get(lineY).stream()
            .mapToLong(segment -> {
                var segmentLength = segment.length();
                for (Device device : devicesOnLine) {
                    if (segment.contains(device.x)) {
                        segmentLength -= 1;
                    }
                }
                return segmentLength;
            })
            .sum();
    }

    private void drawSensingZone(Sensor sensor) {
        int distanceToBeacon = manhattanDistance(sensor, sensor.closestBeacon());

        for (long y = sensor.y(), lineLength = distanceToBeacon; y >= sensor.y() - distanceToBeacon; y--, lineLength--) {
            if (targetLine.isEmpty() || y == targetLine.get()) {
                var newSegment = new Segment(sensor.x() - lineLength, sensor.x() + lineLength, y);
                addSegment(newSegment);
            }
        }
        for (long y = sensor.y(), lineLength = distanceToBeacon; y <= sensor.y() + distanceToBeacon; y++, lineLength--) {
            if (targetLine.isEmpty() || y == targetLine.get()) {
                var newSegment = new Segment(sensor.x() - lineLength, sensor.x() + lineLength, y);
                addSegment(newSegment);
            }
        }
    }

    private void addSegment(Segment newSegment) {
        var segmentsOnLine = segmentsByLine.computeIfAbsent(newSegment.y, y -> new LinkedList<>());

        if (segmentsOnLine.isEmpty()) {
            segmentsOnLine.add(newSegment);
        } else {
            var addSegment = true;
            for (int i = 0; i < segmentsOnLine.size(); i++) {
                var segment = segmentsOnLine.get(i);
                if (segment.overlaps(newSegment)) {
                    segmentsOnLine.remove(i);
                    segmentsOnLine.add(i, segment.joinWith(newSegment));
                    addSegment = false;
                    break;
                }
            }

            if (addSegment) {
                segmentsOnLine.add(newSegment);
            }
            segmentsOnLine.sort(Comparator.comparing(Segment::startX));

            for (int i = 0; i < segmentsOnLine.size() - 1; ) {
                var s1 = segmentsOnLine.get(i);
                var s2 = segmentsOnLine.get(i + 1);

                if (s1.overlaps(s2)) {
                    segmentsOnLine.remove(i + 1);
                    segmentsOnLine.remove(i);
                    segmentsOnLine.add(i, s1.joinWith(s2));
                } else {
                    i++;
                }
            }
        }

    }

    private int manhattanDistance(Sensor sensor, Beacon closestBeacon) {
        return Math.abs(sensor.x() - closestBeacon.x()) + Math.abs(sensor.y() - closestBeacon.y());
    }

    public Device findDistressBeacon(int maxCoordinate) {
        return segmentsByLine.keySet().stream()
            .filter(line -> line >= 0 && line <= maxCoordinate)
            .map(segmentsByLine::get)
            .filter(segments -> !segments.isEmpty())
            .map(segments -> emptySlotLowerThan(segments, maxCoordinate))
            .filter(Optional::isPresent)
            .findFirst()
            .orElseThrow()
            .orElseThrow()
            ;
    }

    private Optional<Device> emptySlotLowerThan(List<Segment> segments, int maxCoordinate) {
        segments.sort(Comparator.comparing(Segment::startX));

        long x = segments.get(0).startX;

        for (Segment s : segments) {
            if (x < s.startX && x >= 0) {
                return Optional.of(new Device(x, s.y));
            } else {
                x = s.endX + 1;
            }

            if (x > maxCoordinate) break;
        }

        return Optional.empty();
    }

    public record Device(long x, long y) {

        public long tuningFrequency() {
            return x * 4000000 + y;
        }
    }

    public record Segment(long startX, long endX, long y) {

        public boolean overlaps(Segment other) {
            return (startX <= other.startX && endX >= other.startX)
                || (startX <= other.endX && endX >= other.endX)
                // other overlaps this
                || (other.startX <= startX && other.endX >= startX)
                || (other.startX <= endX && other.endX >= endX);
        }

        public Segment joinWith(Segment other) {
            return new Segment(
                Math.min(startX, other.startX),
                Math.max(endX, other.endX),
                y
            );
        }

        public long length() {
            return endX - startX + 1;
        }

        public boolean contains(long x) {
            return startX <= x && endX >= x;
        }
    }
}
