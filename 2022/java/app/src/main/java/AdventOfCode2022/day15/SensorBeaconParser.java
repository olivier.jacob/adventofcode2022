package AdventOfCode2022.day15;

import java.util.List;
import java.util.regex.Pattern;

public class SensorBeaconParser {
    private static final Pattern SENSOR_LINE_PATTERN = Pattern.compile("Sensor at x=(-?\\d+), y=(-?\\d+): closest beacon is at x=(-?\\d+), y=(-?\\d+)");

    public static List<Sensor> parseLines(List<String> sensorLines) {
        return sensorLines.stream().map(sensorLine -> {
            var matcher = SENSOR_LINE_PATTERN.matcher(sensorLine);
            if (matcher.matches()) {
                var sensorX = Integer.parseInt(matcher.group(1));
                var sensorY = Integer.parseInt(matcher.group(2));
                var beaconX = Integer.parseInt(matcher.group(3));
                var beaconY = Integer.parseInt(matcher.group(4));

                return new Sensor(sensorX, sensorY, new Beacon(beaconX, beaconY));
            } else {
                throw new RuntimeException("Parse error on line: " + sensorLine);
            }
        }).toList();
    }
}
