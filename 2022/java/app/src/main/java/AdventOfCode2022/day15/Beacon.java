package AdventOfCode2022.day15;

public record Beacon(int x, int y) {
}
