package AdventOfCode2022.day15;

public record Sensor(int x, int y, Beacon closestBeacon) {
}
