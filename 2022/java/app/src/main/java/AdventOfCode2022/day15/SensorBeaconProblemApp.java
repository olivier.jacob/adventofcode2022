package AdventOfCode2022.day15;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class SensorBeaconProblemApp {
    public static void main(String[] args) throws IOException {
        var app = new SensorBeaconProblemApp();
        List<String> lines = app.readInput("day15/input.txt");

        var sensors = SensorBeaconParser.parseLines(lines);
        var problem = new SensorBeaconProblem(2000000);
        problem.detectBeaconLessSpots(sensors);

        System.out.println("Beacon less spots: " + problem.countBeaconLessSpotsOnLine(2000000));

        problem = new SensorBeaconProblem();
        problem.detectBeaconLessSpots(sensors);
        var distressBeacon = problem.findDistressBeacon(4000000);

        System.out.println("Distress beacon: " + distressBeacon);
        System.out.println("Tuning frequency: " + distressBeacon.tuningFrequency());
    }

    private List<String> readInput(String filename) throws IOException {
        var path = Path.of(this.getClass().getClassLoader().getResource(filename).getPath());
        return Files.readAllLines(path);
    }
}
