package AdventOfCode2022.day13;

import AdventOfCode2022.day13.Packet.Val;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PacketOrderingProblemPart2 {
    private List<Packet> packets;

    public static void main(String[] args) throws IOException {
        var problem = new PacketOrderingProblemPart2();
        problem.loadInput("day13/input.txt");


        var divider2 = Packet.of(Packet.of(new Val(2)));
        var divider6 = Packet.of(Packet.of(new Val(6)));
        problem.addDividerPackets(divider2, divider6);
        List<Packet> sortedPackets = problem.sortPackets();

        sortedPackets.stream().map(Packet::toString).forEach(System.out::println);

        var div2Idx = sortedPackets.indexOf(divider2) + 1;
        var div6Idx = sortedPackets.indexOf(divider6) + 1;

        System.out.println("Decoder key " + (div2Idx * div6Idx));
    }

    private void addDividerPackets(Packet... dividerPackets) {
        for (Packet dividerPacket : dividerPackets) {
            this.packets.add(dividerPacket);
        }
    }

    private List<Packet> sortPackets() {
        return this.packets.stream().sorted().toList();
    }

    private void loadInput(String filename) throws IOException {
        var path = Path.of(this.getClass().getClassLoader().getResource(filename).getPath());
        var content = Files.readAllLines(path).stream().filter(Predicate.not(String::isBlank)).toList();

        var parser = new PacketOrderingParser();

        this.packets = new ArrayList(content.stream().map(parser::parseLine).toList());
    }
}
