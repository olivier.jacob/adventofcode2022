package AdventOfCode2022.day13;

public record PacketPair(int index, Packet left, Packet right) {
    public boolean isOrdered() {
        return left.compareTo(right) < 0;
    }
}
