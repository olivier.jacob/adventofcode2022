package AdventOfCode2022.day13;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class PacketOrderingProblem {
    private List<PacketPair> pairs;

    public static void main(String[] args) throws IOException {
        var problem = new PacketOrderingProblem();
        problem.loadInput("day13/input.txt");

        List<PacketPair> orderedPairs = problem.getOrderedPairs();

        var sumOfIndex = orderedPairs.stream().mapToInt(PacketPair::index).sum();
        System.out.println("Sum is " + sumOfIndex);
    }

    private List<PacketPair> getOrderedPairs() {
        return this.pairs.stream().filter(PacketPair::isOrdered).toList();
    }

    private void loadInput(String filename) throws IOException {
        var path = Path.of(this.getClass().getClassLoader().getResource(filename).getPath());
        var content = Files.readString(path);

        var parser = new PacketOrderingParser();

        this.pairs = new ArrayList<>();
        var pairTexts = content.split("\\n\\n");
        for (int i = 0; i < pairTexts.length; i++) {
            this.pairs.add(parser.parsePairText(pairTexts[i], i + 1));
        }
    }
}
