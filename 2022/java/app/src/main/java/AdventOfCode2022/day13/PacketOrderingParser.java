package AdventOfCode2022.day13;

import AdventOfCode2022.day13.Packet.Val;

import java.util.ArrayDeque;
import java.util.Deque;

public class PacketOrderingParser {

    public PacketPair parsePacketPair(int index, String left, String right) {
        return new PacketPair(
            index, parseLine(left), parseLine(right)
        );
    }
    public Packet parseLine(String line) {
        var packetContent = line.substring(1, line.length() - 1);

        Deque<Packet> subs = new ArrayDeque<>();
        subs.push(new Packet());

        for (int i = 0; i < packetContent.length(); i++) {
            var c = packetContent.charAt(i);
            if (c == ',') {
                continue;
            } else if (c == '[') {
                subs.push(new Packet());
            } else if (c == ']') {
                var subPacket = subs.pop();
                subs.peek().addItem(subPacket);
            } else {
                var nextBreakIndex = nextBreakIndex(packetContent, i);
                var num = packetContent.substring(i, nextBreakIndex);
                subs.peek().addItem(new Val(Integer.parseInt(num.isEmpty() ? String.valueOf(c) : num)));

                i = nextBreakIndex - 1;
            }
        }

        return subs.pop();
    }

    private int nextBreakIndex(String content, int startIndex) {
        for (int j = startIndex + 1; j < content.length(); j++) {
            char c = content.charAt(j);
            if (c == ',' || c == ']') {
                return j;
            }
        }
        return content.length();
    }

    public PacketPair parsePairText(String pairText, int index) {
        var elts = pairText.split("\\n");
        return new PacketPair(
            index,
            this.parseLine(elts[0]),
            this.parseLine(elts[1])
        );
    }
}
