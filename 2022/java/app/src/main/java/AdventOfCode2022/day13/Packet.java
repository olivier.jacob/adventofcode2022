package AdventOfCode2022.day13;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Packet implements PacketItem {
    private List<PacketItem> items = new ArrayList<>();

    public void addItem(PacketItem item) {
        items.add(item);
    }

    @Override
    public String toString() {
        return String.format("[%s]", items.stream().map(Objects::toString).collect(Collectors.joining(",")));
    }

    @Override
    public int compareTo(PacketItem right) {
        if (right instanceof Val rVal) {
            return this.compareTo(rVal.asSubPacket());
        }

        var rightPacketItems = ((Packet) right).items;

        int i = 0;
        for (; i < items.size() && i < rightPacketItems.size(); i++) {
            var l = items.get(i);
            var r = rightPacketItems.get(i);

            var compareResult = l.compareTo(r);
            if (compareResult != 0) {
                return compareResult;
            }
        }

        return items.size() - rightPacketItems.size();
    }

    public static class Val implements PacketItem {
        private int digit;

        public Val(int digit) {
            this.digit = digit;
        }

        @Override
        public String toString() {
            return String.valueOf(digit);
        }

        @Override
        public int compareTo(PacketItem right) {
            if (right instanceof Val rVal) {
                return this.digit - rVal.digit;
            } else {
                return this.asSubPacket().compareTo(right);
            }
        }

        private Packet asSubPacket() {
            return Packet.of(this);
        }
    }

    public static Packet of(PacketItem... items) {
        var p = new Packet();

        for (PacketItem item : items) {
            p.addItem(item);
        }

        return p;
    }
}
