package AdventOfCode2022.day8;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class TreeHouseProblem {
    private Forest forest;

    public static void main(String[] args) throws IOException {
        var problem = new TreeHouseProblem();
        problem.loadFrom("day8/input.txt");

        System.out.println("Visible trees : " + problem.countVisibleTrees());
        System.out.println("Highest scenic score : " + problem.highestScenicScore());
    }

    private long highestScenicScore() {
        return forest.highestScenicScore();
    }

    private long countVisibleTrees() {
        return forest.countVisibleTrees();
    }

    private void loadFrom(String filename) throws IOException {
        var path = Path.of(this.getClass().getClassLoader().getResource(filename).getPath());
        var inputForest = Files.readAllLines(path);
        this.forest = new Forest(inputForest);
    }
}
