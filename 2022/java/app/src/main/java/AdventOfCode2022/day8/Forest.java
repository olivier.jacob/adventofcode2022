package AdventOfCode2022.day8;

import java.util.List;

public class Forest {
    private int[][] trees;
    private int width;
    private int depth;

    public Forest(List<String> input) {
        parseInput(input);
    }

    private void parseInput(List<String> input) {
        this.depth = input.size();
        this.width = input.get(0).length();

        trees = new int[depth][width];

        for (int row = 0; row < depth; row++) {
            var treesRow = input.get(row);
            for (int col = 0; col < treesRow.length(); col++) {
                trees[row][col] = treesRow.charAt(col) - '0';
            }
        }
    }

    public int getTree(int row, int col) {
        return trees[row][col];
    }

    public boolean isTreeVisible(int row, int col) {
        return isVisibleWithVector(row, col, ViewVector.left())
            || isVisibleWithVector(row, col, ViewVector.right())
            || isVisibleWithVector(row, col, ViewVector.up())
            || isVisibleWithVector(row, col, ViewVector.down());
    }

    public long countVisibleTrees() {
        long count = 0;
        for (int i = 0; i < this.depth; i++) {
            for (int j = 0; j < this.width; j++) {
                if (isTreeVisible(i, j)) {
                    count++;
                }
            }
        }

        return count;
    }

    private boolean isVisibleWithVector(int row, int col, ViewVector vector) {
        int candidateTree = getTree(row, col);
        if (vector.rowDir != 0) {
            for (int currentRow = row + vector.rowDir; currentRow >= 0 && currentRow < this.depth; currentRow += vector.rowDir) {
                if (candidateTree <= getTree(currentRow, col)) {
                    return false;
                }
            }
            return true;
        } else {
            for (int currentColumn = col + vector.colDir; currentColumn >= 0 && currentColumn < this.width; currentColumn += vector.colDir()) {
                if (candidateTree <= getTree(row, currentColumn)) {
                    return false;
                }
            }
            return true;
        }
    }

    public long scenicScore(int row, int col) {
        return viewingDistance(row, col, ViewVector.left())
            * viewingDistance(row, col, ViewVector.right())
            * viewingDistance(row, col, ViewVector.up())
            * viewingDistance(row, col, ViewVector.down());
    }



    private long viewingDistance(int row, int col, ViewVector vector) {
        long viewingDistance = 0;
        int candidateTree = getTree(row, col);
        if (vector.rowDir != 0) {
            for (int currentRow = row + vector.rowDir; currentRow >= 0 && currentRow < this.depth; currentRow += vector.rowDir) {
                viewingDistance++;
                if (candidateTree <= getTree(currentRow, col)) break;
            }
        } else {
            for (int currentColumn = col + vector.colDir; currentColumn >= 0 && currentColumn < this.width; currentColumn += vector.colDir()) {
                viewingDistance++;
                if (candidateTree <= getTree(row, currentColumn)) break;
            }
        }

        return viewingDistance;
    }

    public long highestScenicScore() {
        long highestScore = 0;
        for (int i = 0; i < this.depth; i++) {
            for (int j = 0; j < this.width; j++) {
                highestScore = Math.max(highestScore, scenicScore(i, j));
            }
        }

        return highestScore;
    }

    private record ViewVector(int rowDir, int colDir) {

        static ViewVector left() { return new ViewVector(0, -1); }
        static ViewVector right() { return new ViewVector(0, 1); }
        static ViewVector up() { return new ViewVector(-1, 0); }
        static ViewVector down() { return new ViewVector(1, 0); }
    }
}
