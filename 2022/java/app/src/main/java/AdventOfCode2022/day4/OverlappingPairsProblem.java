package AdventOfCode2022.day4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class OverlappingPairsProblem {
    public static void main(String[] args) throws IOException {
        var problem = new OverlappingPairsProblem();
        var pairs = problem.loadPairsFrom("day4/input.txt");
        var countOverlapping = problem.countOverlapping(pairs);
        var countNotOverlapping = problem.countSimplyOverlapping(pairs);


        System.out.println("Overlapping pairs count " + countOverlapping);
        System.out.println("Not overlapping pairs count " + countNotOverlapping);
    }

    private List<SectionPair> loadPairsFrom(String filename) throws IOException {
        var path = Path.of(this.getClass().getClassLoader().getResource(filename).getPath());
        var lines = Files.readAllLines(path);

        return fromPairs(lines);
    }

    public List<SectionPair> fromPairs(List<String> pairsRepresentations) {
        return pairsRepresentations.stream()
            .map(SectionPair::parseSectionPairs)
            .collect(Collectors.toList());
    }

    public long countOverlapping(List<SectionPair> sectionPairs) {
        return sectionPairs.stream()
            .filter(SectionPair::areOverlapping)
            .count();
    }

    public long countSimplyOverlapping(List<SectionPair> sectionPairs) {
        return sectionPairs.stream()
            .filter(SectionPair::simplyOverlapping)
            .count();
    }
}
