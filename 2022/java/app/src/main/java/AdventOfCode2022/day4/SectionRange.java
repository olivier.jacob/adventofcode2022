package AdventOfCode2022.day4;

public record SectionRange(int lower, int upper) {
    public boolean isOverlapping(SectionRange other) {
        return this.lower <= other.lower() && this.upper >= other.upper();
    }

    public boolean simplyOverlapping(SectionRange other) {
        return (other.contains(this.lower) && !other.contains(this.upper))
            || (!other.contains(this.lower) && other.contains(this.upper));
    }

    private boolean contains(int slot) {
        return this.lower <= slot && slot <= this.upper;
    }
}
