package AdventOfCode2022.day4;

public record SectionPair(SectionRange r1, SectionRange r2) {
    public boolean areOverlapping() {
        return r1.isOverlapping(r2) || r2.isOverlapping(r1);
    }

    public boolean simplyOverlapping() {
        return areOverlapping() || areSimplyOverlapping();
    }

    private boolean areSimplyOverlapping() {
        return r1.simplyOverlapping(r2) && r2.simplyOverlapping(r1);
    }

    public static SectionPair parseSectionPairs(String pairsRepresentation) {
        var pairs = pairsRepresentation.split(",");

        return new SectionPair(
            parsePair(pairs[0]),
            parsePair(pairs[1])
        );
    }

    private static SectionRange parsePair(String pairRepresentation) {
        var pair = pairRepresentation.split("-");

        return new SectionRange(Integer.parseInt(pair[0]), Integer.parseInt(pair[1]));
    }
}
