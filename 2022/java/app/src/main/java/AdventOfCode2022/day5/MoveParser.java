package AdventOfCode2022.day5;

import AdventOfCode2022.day5.CratesLoadingProblem.Move;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MoveParser {
    private String filename;

    public MoveParser(String filename) {this.filename = filename;}

    public static Move parse(String moveRepresentation) {
        String moveRegex = "move (\\d+) from (\\d+) to (\\d+)";

        var pattern = Pattern.compile(moveRegex);
        var matcher = pattern.matcher(moveRepresentation);

        if (!matcher.find()) {
            throw new RuntimeException(String.format("Parse error. Unable to parse string <%s>", moveRepresentation));
        }

        return new Move(
            Integer.parseInt(matcher.group(1)),
            Integer.parseInt(matcher.group(2)),
            Integer.parseInt(matcher.group(3))
        );
    }

    public List<Move> parse() {
        return null;
    }


}
