package AdventOfCode2022.day5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class CratesLoadingProblem {
    private ArrayList<Stack<Character>> stacks;

    public static void main(String[] args) throws IOException {
        ArrayList<Stack<Character>> stacks = new ArrayList<>(3);

        stacks.add(0, stackWith('S','Z','P','D','L','B','F','C'));
        stacks.add(1, stackWith('N','V','G','P','H','W','B'));
        stacks.add(2, stackWith('F','W','B','J','G'));
        stacks.add(3, stackWith('G','J','N','F','L','W','C','S'));
        stacks.add(4, stackWith('W','J','L','T','P','M','S','H'));
        stacks.add(5, stackWith('B','C','W','G','F','S'));
        stacks.add(6, stackWith('H','T','P','M','Q','B','W'));
        stacks.add(7, stackWith('F','S','W','T'));
        stacks.add(8, stackWith('N','C','R'));

        var problem = new CratesLoadingProblem(stacks);
        List<Move> moves = problem.loadMoves("day5/input.txt");

        moves.forEach(problem::applyMove);
        System.out.println("Message to send : <" + problem.topStackItems() + ">");
    }

    public CratesLoadingProblem(ArrayList<Stack<Character>> stacks) {
        this.stacks = stacks;
    }

    public List<Move> loadMoves(String filename) throws IOException {
        var path = Path.of(this.getClass().getClassLoader().getResource(filename).getPath());
        return Files.readAllLines(path).stream()
            .map(MoveParser::parse)
            .toList();
    }

    public void applyMove(Move m) {
        var fromStack = stacks.get(m.from() - 1);
        var toStack = stacks.get(m.to() - 1);

        var bufferingStack = new Stack<Character>();
        for (int i = 0; i < m.quantity(); i++) {
            bufferingStack.push(fromStack.pop());
        }
        for (int i = 0; i < m.quantity(); i++) {
            toStack.push(bufferingStack.pop());
        }
    }

    public static Stack<Character> stackWith(Character... items) {
        Stack<Character> stack = new Stack<>();

        for (Character c : items) {
            stack.push(c);
        }

        return stack;
    }

    public String topStackItems() {
        return stacks.stream().map(Stack::peek)
            .reduce("", (acc, s) -> acc + s, (s1, s2) -> s1 + s2);
    }

    record Move(int quantity, int from, int to) {
        public static Move of(int quantity, int from, int to) {
            return new Move(quantity, from, to);
        }
    }
}
