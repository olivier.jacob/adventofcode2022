package AdventOfCode2022.day2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public class RockPaperScisors {
    public static void main(String[] args) throws IOException {
        var score = new RockPaperScisors().score("day2/input.txt");
        System.out.println("Score is : " + score);
    }

    public int score(String filepath) throws IOException {
        var path = Path.of(this.getClass().getClassLoader().getResource(filepath).getPath());
        var lines = Files.readAllLines(path);

        System.out.println("There are " + lines.size() + " rounds registered");

        return score(lines);
    }

    public int score(List<String> lines) {
        return lines.stream()
            .map(this::asRound)
            .map(Round::total)
            .mapToInt(Integer::intValue)
            .sum();
    }

    public Round asRound(String roundAsString) {
        var shapes = roundAsString.split(" ");

        return new Round(
            mapShape(shapes[0]),
            mapOutcome(shapes[1])
        );
    }

    private Shape mapShape(String shape) {
        final Map<String, Shape> stringToShape = Map.of(
            "A", Shape.ROCK,
            "B", Shape.PAPER,
            "C", Shape.SCISORS
        );

        return stringToShape.get(shape);
    }

    private ExpectedOutcome mapOutcome(String outcome) {
        final Map<String, ExpectedOutcome> stringToOutcome = Map.of(
            "X", ExpectedOutcome.LOSS,
            "Y", ExpectedOutcome.DRAW,
            "Z", ExpectedOutcome.WIN
        );

        return stringToOutcome.get(outcome);
    }

    public enum Shape {
        ROCK(1), PAPER(2), SCISORS(3);

        private final int score;

        Shape(int score) {
            this.score = score;
        }

        public int evaluate() {
            return score;
        }
    }

    public enum ExpectedOutcome {
        LOSS(0), WIN(6), DRAW(3);

        private int score;

        ExpectedOutcome(int score) {
            this.score = score;
        }

        public int getScore() {
            return score;
        }
    }

    public record Round(Shape player1, ExpectedOutcome outcome) {

        public int evaluate() {
            return outcome.getScore();
        }

        public Shape determinePlay() {
            return switch (player1) {
                case ROCK -> switch (outcome) {
                    case LOSS -> Shape.SCISORS;
                    case DRAW -> Shape.ROCK;
                    case WIN -> Shape.PAPER;
                };
                case PAPER -> switch (outcome) {
                    case LOSS -> Shape.ROCK;
                    case DRAW -> Shape.PAPER;
                    case WIN -> Shape.SCISORS;
                };
                case SCISORS -> switch (outcome) {
                    case LOSS -> Shape.PAPER;
                    case DRAW -> Shape.SCISORS;
                    case WIN -> Shape.ROCK;
                };
            };
        }

        public int total() {
            return evaluate() + determinePlay().evaluate();
        }
    }
}
