package AdventOfCode2022.day6;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class SignalStartDetectionProblem {

    public static void main(String[] args) throws IOException {
        var problem = new SignalStartDetectionProblem();
        String signal = problem.readSignal("day6/input.txt");

        System.out.println("First marker occurs at " + problem.firstMarkerOccurenceIndex(signal));
        System.out.println("First marker for message occurs at " + problem.firstMessageMarkerOccurence(signal));
    }

    private String readSignal(String filename) throws IOException {
        var path = Path.of(this.getClass().getClassLoader().getResource(filename).getPath());
        return Files.readString(path);
    }

    public boolean isStartMarker(String signal) {
        return signal.chars().distinct().count() == signal.length();
    }

    public int firstMarkerOccurenceIndex(String signal) {
        for (int i = 0, j = 4; j < signal.length(); i++, j++) {
            if (isStartMarker(signal.substring(i, j))) {
                return j;
            }
        }
        throw new RuntimeException("No start marker detected");
    }

    public int firstMessageMarkerOccurence(String signal) {
        for (int i = 0, j = 14; j < signal.length(); i++, j++) {
            if (isStartMarker(signal.substring(i, j))) {
                return j;
            }
        }
        throw new RuntimeException("No start marker for message detected");
    }
}
