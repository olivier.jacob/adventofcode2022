package AdventOfCode2022.day1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class GenericElvesCalories {
    public static void main(String... args) throws IOException {
        int max1 = new GenericElvesCalories().computeSumOfNMax("day1/input.txt", 1);
        int max3 = new GenericElvesCalories().computeSumOfNMax("day1/input.txt", 3);
        int max5 = new GenericElvesCalories().computeSumOfNMax("day1/input.txt", 5);

        System.out.println("Max1 = " + max1);
        System.out.println("Max3 = " + max3);
        System.out.println("Max5 = " + max5);
    }

    public int computeSumOfNMax(String filepath, int nMax) throws IOException {
        var path = Path.of(this.getClass().getClassLoader().getResource(filepath).getPath());
        var lines = Files.readAllLines(path);

        List<Integer> elves = new LinkedList<>();
        int currentElf = 0;
        for (String line : lines) {
            if (line.isBlank()) {
                elves.add(currentElf);
                currentElf = 0;
            } else {
                currentElf += Integer.parseInt(line.trim());
            }
        }

        System.out.println(String.format("There are %d elves", elves.size()));

        // Does not handle size < nMax
        return elves.stream().sorted(Comparator.reverseOrder()).limit(nMax).reduce(Integer::sum).orElse(-1);
    }
}
