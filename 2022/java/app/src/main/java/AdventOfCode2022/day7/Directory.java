package AdventOfCode2022.day7;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Directory implements Node {
    private final String name;
    private final List<Node> children = new ArrayList<>();

    public Directory(String name) {
        this.name = name;
    }


    public String name() {
        return name;
    }

    @Override
    public List<Node> children() {
        return children;
    }

    public void addChild(Node node) {
        children.add(node);
    }

    @Override
    public List<Node> recursiveChildren() {
        return Stream.concat(
            Stream.of(this),
            children.stream()
                .flatMap(child -> child.recursiveChildren().stream())
        ).toList();
    }

    @Override
    public long size() {
        return children.stream().mapToLong(Node::size).sum();
    }

    @Override
    public String toString() {
        return String.format("Dir(%s, %d)", name, size());
    }
}
