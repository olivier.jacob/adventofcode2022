package AdventOfCode2022.day7;

public class OutputTokens {
    public static final EndToken END_TOKEN = new EndToken();

    public static class EndToken implements OutputToken {
        @Override
        public boolean isEndToken() {
            return true;
        }
    }

    public static class ChangeDirectory implements OutputToken {

        private final String targetDirectory;

        public ChangeDirectory(String targetDirectory) {
            this.targetDirectory = targetDirectory;
        }

        public String targetDirectory() {
            return targetDirectory;
        }
    }

    public static class ListContent implements OutputToken {}

    public static class Dir implements OutputToken {
        private final String name;

        public Dir(String name) {
            this.name = name;
        }

        public String name() {
            return name;
        }
    }

    public static class Filesize implements OutputToken {

        private final String name;
        private final int size;

        public Filesize(String name, int size) {
            this.name = name;
            this.size = size;
        }

        public String name() {
            return name;
        }

        public int size() {
            return size;
        }
    }
}
