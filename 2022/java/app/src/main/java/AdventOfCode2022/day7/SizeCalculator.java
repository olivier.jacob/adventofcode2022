package AdventOfCode2022.day7;

import java.util.Comparator;
import java.util.stream.Collectors;

public class SizeCalculator {
    private final Directory dir;

    public SizeCalculator(Directory dir) {
        this.dir = dir;
    }

    public long totalSize() {
        return dir.recursiveChildren().stream()
            .filter(Directory.class::isInstance)
            .mapToLong(Node::size)
            .filter(size -> size <= 100000)
            .sum();
    }

    public Directory smallestWithSizeBiggerThan(long minimumSize) {
        return (Directory) dir.recursiveChildren().stream()
            .filter(Directory.class::isInstance)
            .filter(node -> node.size() >= minimumSize)
            .min(Comparator.comparing(Node::size))
            .orElse(dir);
    }
}
