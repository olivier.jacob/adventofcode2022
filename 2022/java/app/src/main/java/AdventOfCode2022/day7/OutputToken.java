package AdventOfCode2022.day7;

public interface OutputToken {
    default boolean isEndToken() {
        return false;
    }
}
