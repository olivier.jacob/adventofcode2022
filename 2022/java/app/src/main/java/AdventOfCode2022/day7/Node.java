package AdventOfCode2022.day7;

import java.util.List;
import java.util.function.Function;

public interface Node {
    List<Node> children();

    List<Node> recursiveChildren();

    default <T> T accept(Function<Node, T> visitor) {
        return visitor.apply(this);
    }

    long size();
}
