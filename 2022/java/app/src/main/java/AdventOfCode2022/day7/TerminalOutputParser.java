package AdventOfCode2022.day7;

import AdventOfCode2022.day7.OutputTokens.ChangeDirectory;
import AdventOfCode2022.day7.OutputTokens.Filesize;

import java.util.Stack;

public class TerminalOutputParser {
    private final TerminalOutputTokenizer tokenizer;
    private Stack<Directory> dirStack = new Stack<>();

    public TerminalOutputParser(TerminalOutputTokenizer tokenizer) {this.tokenizer = tokenizer;}

    public Directory parse() {
        var currentToken = tokenizer.next();

        while (!currentToken.isEndToken()) {
            if (currentToken instanceof ChangeDirectory cd) {
                if (cd.targetDirectory().equals("..")) {
                    var poppedDir = popDirectory();
                    dirStack.peek().addChild(poppedDir);
                } else {
                    pushDirectory(new Directory(cd.targetDirectory()));
                }
            } else if (currentToken instanceof Filesize file) {
                dirStack.peek().addChild(new FileNode(file.name(), file.size()));
            }

            currentToken = tokenizer.next();
        }

        Directory currentDir = dirStack.pop();
        while (!dirStack.empty()) {
            dirStack.peek().addChild(currentDir);
            currentDir = dirStack.pop();
        }

        return currentDir;
    }

    private void pushDirectory(Directory directory) {
        dirStack.push(directory);
    }

    private Directory popDirectory() {
        return dirStack.pop();
    }
}
