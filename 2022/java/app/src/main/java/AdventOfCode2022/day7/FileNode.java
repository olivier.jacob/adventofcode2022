package AdventOfCode2022.day7;

import java.util.List;

public class FileNode implements Node {
    private final String name;
    private final long size;

    public FileNode(String name, long size) {
        this.name = name;
        this.size = size;
    }

    @Override
    public List<Node> children() {
        return List.of();
    }

    @Override
    public List<Node> recursiveChildren() {
        return List.of(this);
    }

    @Override
    public long size() {
        return size;
    }
}
