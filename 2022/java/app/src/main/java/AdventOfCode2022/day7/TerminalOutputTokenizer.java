package AdventOfCode2022.day7;

import AdventOfCode2022.day7.OutputTokens.ChangeDirectory;
import AdventOfCode2022.day7.OutputTokens.Dir;
import AdventOfCode2022.day7.OutputTokens.Filesize;
import AdventOfCode2022.day7.OutputTokens.ListContent;

import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public class TerminalOutputTokenizer {
    private final List<String> outputLines;
    private final Iterator<String> lineIterator;

    public TerminalOutputTokenizer(List<String> outputLines) {
        this.outputLines = outputLines;
        this.lineIterator = outputLines.iterator();
    }

    public OutputToken next() {
        if (!lineIterator.hasNext()) {
            return OutputTokens.END_TOKEN;
        }

        String output = lineIterator.next();

        if (output.startsWith("$ cd")) {
            return parseChangeDir(output);
        } else if (output.startsWith("$ ls")) {
            return new ListContent();
        } else if (output.startsWith("dir ")) {
            return parseDir(output);
        } else {
            return parseFile(output);
        }
    }

    private OutputToken parseChangeDir(String output) {
        var pattern = Pattern.compile("\\$ cd ([a-zA-Z_0-9/\\.]+)");
        var matcher = pattern.matcher(output);

        if (matcher.matches()) {
            var targetDir = matcher.group(1);
            return new ChangeDirectory(targetDir);
        }

        throw new RuntimeException("Parse error (" + output + ")");
    }

    private OutputToken parseDir(String output) {
        var pattern = Pattern.compile("dir ([a-zA-Z_0-9/]+)");
        var matcher = pattern.matcher(output);

        if (matcher.matches()) {
            var dirName = matcher.group(1);
            return new Dir(dirName);
        }

        throw new RuntimeException("Parse error (" + output + ")");
    }

    private OutputToken parseFile(String output) {
        var pattern = Pattern.compile("(\\d+) ([a-zA-Z_0-9\\.]+)");
        var matcher = pattern.matcher(output);

        if (matcher.matches()) {
            var size = Integer.parseInt(matcher.group(1));
            var filename = matcher.group(2);
            return new Filesize(filename, size);
        }

        throw new RuntimeException("Parse error (" + output + ")");
    }
}
