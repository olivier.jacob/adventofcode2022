package AdventOfCode2022.day7;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class DiskCleanerProblem {
    private final List<String> output;

    public DiskCleanerProblem(String filename) throws IOException {
        var path = Path.of(this.getClass().getClassLoader().getResource(filename).getPath());
        this.output = Files.readAllLines(path);
    }

    public static void main(String[] args) throws IOException {
        var problem = new DiskCleanerProblem("day7/input.txt");

        // System.out.println("Total size is " + problem.totalSize());

        var rootDir = problem.parse();

        long totalDiskSpace = 70000000;
        long requiredUnused = 30000000;

        long remainingAvailable = totalDiskSpace - rootDir.size();
        long requiredFreedUp = requiredUnused - remainingAvailable;

        var directory = problem.determineDirToDelete(requiredFreedUp);
        System.out.println("Directory to delete is <" + directory + ">");
    }

    private Directory determineDirToDelete(long minimumSize) {
        Directory directory = parse();
        return new SizeCalculator(directory).smallestWithSizeBiggerThan(minimumSize);
    }

    private long totalSize() {
        Directory directory = parse();
        return new SizeCalculator(directory).totalSize();
    }

    private Directory parse() {
        var parser = new TerminalOutputParser(new TerminalOutputTokenizer(this.output));

        return parser.parse();
    }
}
