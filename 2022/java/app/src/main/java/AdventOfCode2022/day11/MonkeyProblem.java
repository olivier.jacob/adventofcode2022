package AdventOfCode2022.day11;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Comparator;
import java.util.List;

public class MonkeyProblem {
    public static final boolean RELIEF_ACTIVATED = false;
    private static final int NUMBER_OF_ROUNDS = 10000;

    private final List<Monkey> monkeys;
    private final Long commonDivider;

    public MonkeyProblem(List<Monkey> monkeys, Long commonDivider) {
        this.monkeys = monkeys;
        this.commonDivider = commonDivider;
    }

    public static void main(String[] args) throws IOException {
        var parser = new MonkeyProblemParser("day11/input.txt");
        var problem = parser.parseProblem();

        problem.simulateRounds(NUMBER_OF_ROUNDS);
        var monkeyBusiness = problem.computeMonkeyBusiness();

        System.out.println("Monkey business: " + monkeyBusiness);
    }

    private BigInteger computeMonkeyBusiness() {
        return monkeys.stream().map(Monkey::getInspectedItems)
            .sorted(Comparator.reverseOrder())
            .limit(2)
            .reduce(BigInteger.ONE, BigInteger::multiply);
    }

    private void simulateRounds(int numberOfRounds) {
        for (int r = 0; r < numberOfRounds; r++) {
            monkeys.forEach(m -> m.doRound(monkeys, commonDivider));
        }
    }
}
