package AdventOfCode2022.day11;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.LongUnaryOperator;

public class Monkey {
    private final LongUnaryOperator stressOperation;
    private final Function<Long, Integer> throwToMonkey;
    private LinkedList<Item> items;
    private BigInteger inspectedItems = BigInteger.ZERO;

    public Monkey(LongUnaryOperator stressOperation, Function<Long, Integer> throwToMonkey, LinkedList<Item> items) {
        this.stressOperation = stressOperation;
        this.throwToMonkey = throwToMonkey;
        this.items = items;
    }

    public BigInteger getInspectedItems() {
        return inspectedItems;
    }

    public void receive(Item item) {
        this.items.addLast(item);
    }

    public void doRound(List<Monkey> monkeys, Long commonDivider) {
        while (!items.isEmpty()) {
            inspect(commonDivider);
            relief(commonDivider);
            throwToNextMonkey(monkeys);
        }
    }

    private void inspect(Long commonDivider) {
        var item = items.getFirst();
        item.apply(this.stressOperation, commonDivider);
        inspectedItems = inspectedItems.add(BigInteger.ONE);
    }

    private void relief(Long commonDivider) {
        if (MonkeyProblem.RELIEF_ACTIVATED) {
            var item = items.getFirst();
            item.apply(stress -> stress / 3, commonDivider);
        }
    }

    private void throwToNextMonkey(List<Monkey> monkeys) {
        var item = items.removeFirst();

        var nextMonkey = this.throwToMonkey.apply(item.getStressLevel());
        monkeys.get(nextMonkey).receive(item);
    }
}
