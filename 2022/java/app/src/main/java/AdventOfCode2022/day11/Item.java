package AdventOfCode2022.day11;

import java.util.function.LongUnaryOperator;

public class Item {
    private Long stressLevel;

    public Item(Long stressLevel) {
        this.stressLevel = stressLevel;
    }

    public Long getStressLevel() {
        return stressLevel;
    }

    public void apply(LongUnaryOperator operation, Long commonDivider) {
        this.stressLevel = operation.applyAsLong(this.stressLevel) % commonDivider;
    }
}
