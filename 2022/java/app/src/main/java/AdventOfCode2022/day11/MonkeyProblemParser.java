package AdventOfCode2022.day11;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.LongUnaryOperator;
import java.util.regex.Pattern;

public class MonkeyProblemParser {
    private final String content;

    private static final Pattern ITEMS_PATTERN = Pattern.compile("Starting items: (.*)");
    private static final Pattern STRESS_OP_PATTERN = Pattern.compile("Operation: new = old ([\\+\\-\\/\\*]) ([0-9a-z]+)$");
    private static final Pattern TEST_PATTERN = Pattern.compile("Test: divisible by ([0-9]+)$");
    private static final Pattern TRUE_MONKEY_PATTERN = Pattern.compile("If true: throw to monkey ([0-9]+)$");
    private static final Pattern FALSE_MONKEY_PATTERN = Pattern.compile("If false: throw to monkey ([0-9]+)$");
    private Long commonDivider = 1l;


    public MonkeyProblemParser(String filename) throws IOException {
        var path = Path.of(this.getClass().getClassLoader().getResource(filename).getPath());
        this.content = Files.readString(path);
    }

    public List<Monkey> parseMonkeys() {
        var monkeyDefinitions = content.split("\\n\\n");

        return Arrays.stream(monkeyDefinitions).map(this::toMonkey)
            .toList();
    }

    private Monkey toMonkey(String monkeyDefinition) {
        var lines = monkeyDefinition.split("\\n");

        var items = parseItems(lines[1].trim());
        var operation = parseStressOperation(lines[2].trim());
        var throwTest = parseThrowTest(lines[3].trim(), lines[4].trim(), lines[5].trim());

        return new Monkey(operation, throwTest, items);
    }

    private LinkedList<Item> parseItems(String itemsLine) {
        var matcher = ITEMS_PATTERN.matcher(itemsLine);
        if (matcher.matches()) {
            return new LinkedList<>(
                Arrays.stream(matcher.group(1).split(", ")).map(s -> new Item(Long.parseLong(s))).toList()
            );
        }
        throw new RuntimeException("Cannot parse items. Parse error on line: " + itemsLine);
    }

    private LongUnaryOperator parseStressOperation(String stressOperationLine) {
        var matcher = STRESS_OP_PATTERN.matcher(stressOperationLine);
        if (matcher.matches()) {
            var operator = matcher.group(1);
            LongUnaryOperator value = (stress -> matcher.group(2).equals("old") ? stress : Long.parseLong(matcher.group(2)));

            return switch (operator) {
                case "+" -> (stress -> stress + value.applyAsLong(stress));
                case "-" -> (stress -> stress - value.applyAsLong(stress));
                case "/" -> (stress -> stress / value.applyAsLong(stress));
                case "*" -> (stress -> stress * value.applyAsLong(stress));
                default -> throw new RuntimeException("Cannot parse stress operation. Parse error on line: " + stressOperationLine);
            };
        }
        throw new RuntimeException("Cannot parse stress operation. Parse error on line: " + stressOperationLine);
    }

    private Function<Long, Integer> parseThrowTest(String testLine, String monkeyTrueLine, String monkeyFalseLine) {
        var testMatcher = TEST_PATTERN.matcher(testLine);
        var trueMonkeyMatcher = TRUE_MONKEY_PATTERN.matcher(monkeyTrueLine);
        var falseMonkeyMatcher = FALSE_MONKEY_PATTERN.matcher(monkeyFalseLine);

        if (testMatcher.matches() && trueMonkeyMatcher.matches() && falseMonkeyMatcher.matches()) {
            var divider = Integer.parseInt(testMatcher.group(1));
            var trueMonkey = Integer.parseInt(trueMonkeyMatcher.group(1));
            var falseMonkey = Integer.parseInt(falseMonkeyMatcher.group(1));

            this.commonDivider *= divider;
            return stress -> stress % divider == 0 ? trueMonkey : falseMonkey;
        }

        throw new RuntimeException(
            "Cannot parse test operation. Parse error on lines: " +
                String.join("\\n", testLine, monkeyTrueLine, monkeyFalseLine)
            );
    }

    public MonkeyProblem parseProblem() {
        var monkeys = this.parseMonkeys();

        return new MonkeyProblem(monkeys, this.commonDivider);
    }
}
