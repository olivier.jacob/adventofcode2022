package AdventOfCode2022.day2;

import AdventOfCode2022.day2.RockPaperScisors.ExpectedOutcome;
import AdventOfCode2022.day2.RockPaperScisors.Round;
import AdventOfCode2022.day2.RockPaperScisors.Shape;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

class RockPaperScisorsTest {
    @ParameterizedTest
    @MethodSource("stringRepresentationsToRounds")
    void should_create_round_from_string_representation(String representation, Round expectedRound) {
        var game = new RockPaperScisors();
        var round = game.asRound(representation);

        Assertions.assertEquals(expectedRound, round);
    }

    private static Stream<Arguments> stringRepresentationsToRounds() {
        return Stream.of(
            Arguments.of("A Y", new Round(Shape.ROCK, ExpectedOutcome.DRAW)),
            Arguments.of("B X", new Round(Shape.PAPER, ExpectedOutcome.LOSS)),
            Arguments.of("C Z", new Round(Shape.SCISORS, ExpectedOutcome.WIN))
        );
    }

    @Test
    void should_evaluate_round() {
        Assertions.assertEquals(0, new Round(Shape.PAPER, ExpectedOutcome.LOSS).evaluate());
        Assertions.assertEquals(3, new Round(Shape.ROCK, ExpectedOutcome.DRAW).evaluate());
        Assertions.assertEquals(6, new Round(Shape.ROCK, ExpectedOutcome.WIN).evaluate());

        Assertions.assertEquals(0, new Round(Shape.SCISORS, ExpectedOutcome.LOSS).evaluate());
        Assertions.assertEquals(3, new Round(Shape.PAPER, ExpectedOutcome.DRAW).evaluate());
        Assertions.assertEquals(6, new Round(Shape.PAPER, ExpectedOutcome.WIN).evaluate());

        Assertions.assertEquals(0, new Round(Shape.ROCK, ExpectedOutcome.LOSS).evaluate());
        Assertions.assertEquals(3, new Round(Shape.SCISORS, ExpectedOutcome.DRAW).evaluate());
        Assertions.assertEquals(6, new Round(Shape.SCISORS, ExpectedOutcome.WIN).evaluate());
    }

    @Test
    void should_evaluate_shape() {
        Assertions.assertEquals(1, Shape.ROCK.evaluate());
        Assertions.assertEquals(2, Shape.PAPER.evaluate());
        Assertions.assertEquals(3, Shape.SCISORS.evaluate());
    }

    @Test
    void should_determine_what_to_play_according_to_outcome() {
        Assertions.assertEquals(Shape.ROCK, new Round(Shape.PAPER, ExpectedOutcome.LOSS).determinePlay());
        Assertions.assertEquals(Shape.ROCK, new Round(Shape.ROCK, ExpectedOutcome.DRAW).determinePlay());
        Assertions.assertEquals(Shape.ROCK, new Round(Shape.SCISORS, ExpectedOutcome.WIN).determinePlay());

        Assertions.assertEquals(Shape.PAPER, new Round(Shape.SCISORS, ExpectedOutcome.LOSS).determinePlay());
        Assertions.assertEquals(Shape.PAPER, new Round(Shape.PAPER, ExpectedOutcome.DRAW).determinePlay());
        Assertions.assertEquals(Shape.PAPER, new Round(Shape.ROCK, ExpectedOutcome.WIN).determinePlay());

        Assertions.assertEquals(Shape.SCISORS, new Round(Shape.ROCK, ExpectedOutcome.LOSS).determinePlay());
        Assertions.assertEquals(Shape.SCISORS, new Round(Shape.SCISORS, ExpectedOutcome.DRAW).determinePlay());
        Assertions.assertEquals(Shape.SCISORS, new Round(Shape.PAPER, ExpectedOutcome.WIN).determinePlay());
    }

    @Test
    void should_evaluate_round_total() {
        Assertions.assertEquals(1, new Round(Shape.PAPER, ExpectedOutcome.LOSS).total());
        Assertions.assertEquals(3, new Round(Shape.ROCK, ExpectedOutcome.LOSS).total());
        Assertions.assertEquals(4, new Round(Shape.ROCK, ExpectedOutcome.DRAW).total());

        Assertions.assertEquals(6, new Round(Shape.SCISORS, ExpectedOutcome.DRAW).total());
        Assertions.assertEquals(5, new Round(Shape.PAPER, ExpectedOutcome.DRAW).total());
        Assertions.assertEquals(9, new Round(Shape.PAPER, ExpectedOutcome.WIN).total());

        Assertions.assertEquals(8, new Round(Shape.ROCK, ExpectedOutcome.WIN).total());
        Assertions.assertEquals(7, new Round(Shape.SCISORS, ExpectedOutcome.WIN).total());
        Assertions.assertEquals(2, new Round(Shape.SCISORS, ExpectedOutcome.LOSS).total());
    }

    @Test
    void provided_acceptance_test() {
        var lines = List.of("A Y", "B X", "C Z");
        var game = new RockPaperScisors();

        Assertions.assertEquals(12, game.score(lines));
    }

}
