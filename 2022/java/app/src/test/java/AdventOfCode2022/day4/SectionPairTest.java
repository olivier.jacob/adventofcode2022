package AdventOfCode2022.day4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SectionPairTest {
    @Test
    void should_detect_completely_overlaping_pairs() {
        Assertions.assertTrue(
            new SectionPair(
                new SectionRange(2, 8),
                new SectionRange(3, 7)
            ).areOverlapping()
        );
    }

    @Test
    void should_not_detect_partially_overlaping_pairs() {

        Assertions.assertFalse(
            new SectionPair(
                new SectionRange(2, 8),
                new SectionRange(5, 10)
            ).areOverlapping()
        );
    }

    @Test
    void should_not_detect_not_overlaping_pairs() {

        Assertions.assertFalse(
            new SectionPair(
                new SectionRange(2, 8),
                new SectionRange(12, 25)
            ).areOverlapping()
        );
    }

    @Test
    void should_detect_completely_overlaping_pairs_using_both_pairs() {
        Assertions.assertTrue(
            new SectionPair(
                new SectionRange(6, 6),
                new SectionRange(4, 6)
            ).areOverlapping()
        );
    }

    @Test
    void should_detect_simply_overlapping_pairs() {
        Assertions.assertTrue(
            new SectionPair(
                new SectionRange(6, 6),
                new SectionRange(4, 6)
            ).simplyOverlapping()
        );
        Assertions.assertTrue(
            new SectionPair(
                new SectionRange(2, 6),
                new SectionRange(4, 8)
            ).simplyOverlapping()
        );
        Assertions.assertTrue(
            new SectionPair(
                new SectionRange(2, 8),
                new SectionRange(4, 6)
            ).simplyOverlapping()
        );
        Assertions.assertFalse(
            new SectionPair(
                new SectionRange(2, 3),
                new SectionRange(6, 8)
            ).simplyOverlapping()
        );
    }
}
