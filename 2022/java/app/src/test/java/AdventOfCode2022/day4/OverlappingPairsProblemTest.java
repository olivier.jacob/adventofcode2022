package AdventOfCode2022.day4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class OverlappingPairsProblemTest {

    @Test
    void should_create_ranges_from_string_representation() {
        SectionPair pair = SectionPair.parseSectionPairs("2-4,6-8");

        Assertions.assertEquals(
            new SectionPair(new SectionRange(2,4), new SectionRange(6,8)),
            pair
        );
    }

    @Test
    void should_find_all_overlapping_pairs() {
        var problem = new OverlappingPairsProblem();
        var sectionPairs = problem.fromPairs(
            List.of("2-4,6-8", "2-3,4-5", "5-7,7-9", "2-8,3-7", "6-6,4-6", "2-6,4-8")
        );
        var overlappingCount = problem.countOverlapping(sectionPairs);

        Assertions.assertEquals(2, overlappingCount);
    }

    @Test
    void should_find_all_simply_overlapping_pairs() {
        var problem = new OverlappingPairsProblem();
        var sectionPairs = problem.fromPairs(
            List.of("2-4,6-8", "2-3,4-5", "5-7,7-9", "2-8,3-7", "6-6,4-6", "2-6,4-8")
        );
        var simplyOverlappingCount = problem.countSimplyOverlapping(sectionPairs);

        Assertions.assertEquals(4, simplyOverlappingCount);
    }
}
