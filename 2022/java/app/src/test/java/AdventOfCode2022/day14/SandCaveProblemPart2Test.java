package AdventOfCode2022.day14;

import AdventOfCode2022.day14.part2.SandCave;
import AdventOfCode2022.day14.part2.SandCave.CaveCell;
import AdventOfCode2022.day14.part2.SandCaveParser;
import AdventOfCode2022.day14.part2.SandCaveProblemPart2;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

;

class SandCaveProblemPart2Test {

    @Test
    void cave_after_24_units_of_sand() {
        var cave = createSampleCave();

        produceSandTimes(cave, 24);

        Assertions.assertThat(cave).hasToString(
            """
                ......+...
                ..........
                ......o...
                .....ooo..
                ....#ooo##
                ...o#ooo#.
                ..###ooo#.
                ....oooo#.
                .o.ooooo#.
                #########.
                ..........
                ##########
                """
        );
    }

    @Test
    void cave_after_25_units_of_sand() {
        var cave = createSampleCave();

        produceSandTimes(cave, 25);

        Assertions.assertThat(cave).hasToString(
            """
                ........+...
                ............
                ........o...
                .......ooo..
                ......#ooo##
                .....o#ooo#.
                ....###ooo#.
                ......oooo#.
                ...o.ooooo#.
                ..#########.
                .o..........
                ############
                """
        );
    }

    @Test
    void cave_after_93_units_of_sand() {
        var cave = createSampleCave();

        produceSandTimes(cave, 93);

        Assertions.assertThat(cave).hasToString(
            """
                ...........o...........
                ..........ooo..........
                .........ooooo.........
                ........ooooooo........
                .......oo#ooo##o.......
                ......ooo#ooo#ooo......
                .....oo###ooo#oooo.....
                ....oooo.oooo#ooooo....
                ...oooooooooo#oooooo...
                ..ooo#########ooooooo..
                .ooooo.......ooooooooo.
                #######################
                """
        );
    }

    private int produceSandTimes(SandCave cave, int times) {
        for (int i = 1; i <= times; i++) {
            System.out.println("producing unit #" + i);
            if (cave.produceSandUnit().equals(new CaveCell(500 - cave.getOriginX(), 0)))
                return i;
        }
        return times;
    }

    @Test
    void part2_reaches_condition_after_93_units() {
        var cave = createSampleCave();
        var problem = new SandCaveProblemPart2(cave);

        Assertions.assertThat(problem.countSandUnitsBeforeFull()).isEqualTo(93);
    }

    private static SandCave createSampleCave() {
        var lines = List.of(
            "498,4 -> 498,6 -> 496,6",
            "503,4 -> 502,4 -> 502,9 -> 494,9"
        );

        var parser = new SandCaveParser();
        return parser.parse(lines);
    }
}
