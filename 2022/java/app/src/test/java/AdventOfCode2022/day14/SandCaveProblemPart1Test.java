package AdventOfCode2022.day14;

import AdventOfCode2022.day14.part1.SandCave;
import AdventOfCode2022.day14.part1.SandCaveParser;
import AdventOfCode2022.day14.part1.SandCaveProblem;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class SandCaveProblemPart1Test {
    @Test
    void should_create_cave_according_coordinates() {
        SandCave cave = createSampleCave();

        Assertions.assertThat(cave.getOriginX()).isEqualTo(494) ;
        Assertions.assertThat(cave.getOriginY()).isEqualTo(0);
        Assertions.assertThat(cave).hasToString(
            """
                ......+...
                ..........
                ..........
                ..........
                ....#...##
                ....#...#.
                ..###...#.
                ........#.
                ........#.
                #########.
                """
        );
    }

    @Test
    void sand_should_stop_at_wall() {
        var cave = createSampleCave();

        produceSandTimes(cave, 1);

        Assertions.assertThat(cave).hasToString(
            """
                ......+...
                ..........
                ..........
                ..........
                ....#...##
                ....#...#.
                ..###...#.
                ........#.
                ......o.#.
                #########.
                """
        );
    }

    @Test
    void sand_should_move_left_when_falls_on_sand() {
        var cave = createSampleCave();

        produceSandTimes(cave, 2);

        Assertions.assertThat(cave).hasToString(
            """
                ......+...
                ..........
                ..........
                ..........
                ....#...##
                ....#...#.
                ..###...#.
                ........#.
                .....oo.#.
                #########.
                """
        );
    }

    @Test
    void sand_should_move_right_when_left_not_possible() {
        var cave = createSampleCave();

        produceSandTimes(cave, 3);

        Assertions.assertThat(cave).hasToString(
            """
                ......+...
                ..........
                ..........
                ..........
                ....#...##
                ....#...#.
                ..###...#.
                ........#.
                .....ooo#.
                #########.
                """
        );
    }

    @Test
    void sand_should_come_to_rest_when_blocked_left_and_right() {
        var cave = createSampleCave();

        produceSandTimes(cave, 4);

        Assertions.assertThat(cave).hasToString(
            """
                ......+...
                ..........
                ..........
                ..........
                ....#...##
                ....#...#.
                ..###...#.
                ......o.#.
                .....ooo#.
                #########.
                """
        );
    }

    @Test
    void sand_should_keep_moving_while_it_can() {
        var cave = createSampleCave();

        produceSandTimes(cave, 5);

        Assertions.assertThat(cave).hasToString(
            """
                ......+...
                ..........
                ..........
                ..........
                ....#...##
                ....#...#.
                ..###...#.
                ......o.#.
                ....oooo#.
                #########.
                """
        );
    }

    @Test
    void cave_after_22_units_of_sand() {
        var cave = createSampleCave();

        produceSandTimes(cave, 22);

        Assertions.assertThat(cave).hasToString(
            """
                ......+...
                ..........
                ......o...
                .....ooo..
                ....#ooo##
                ....#ooo#.
                ..###ooo#.
                ....oooo#.
                ...ooooo#.
                #########.
                """
        );
    }

    @Test
    void cave_after_24_units_of_sand() {
        var cave = createSampleCave();

        produceSandTimes(cave, 24);

        Assertions.assertThat(cave).hasToString(
            """
                ......+...
                ..........
                ......o...
                .....ooo..
                ....#ooo##
                ...o#ooo#.
                ..###ooo#.
                ....oooo#.
                .o.ooooo#.
                #########.
                """
        );
    }

    @Test
    void sand_flows_into_the_abyss_after_25_units() {
        var cave = createSampleCave();

        Assertions.assertThat(produceSandTimes(cave, 50)).isEqualTo(25);
    }

    @Test
    void should_find_how_many_sand_units_fall_before_overflow() {
        var cave = createSampleCave();
        var problem = new SandCaveProblem(cave);

        Assertions.assertThat(problem.countSandUnitsBeforeOverflow()).isEqualTo(24);
    }

    private int produceSandTimes(SandCave cave, int times) {
        for (int i = 1; i <= times; i++) {
            if (!cave.produceSandUnit())
                return i;
        }
        return times;
    }

    private static SandCave createSampleCave() {
        var lines = List.of(
            "498,4 -> 498,6 -> 496,6",
                "503,4 -> 502,4 -> 502,9 -> 494,9"
        );

        var parser = new SandCaveParser();
        var cave = parser.parse(lines);
        return cave;
    }
}
