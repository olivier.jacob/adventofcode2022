package AdventOfCode2022.day5;

import AdventOfCode2022.day5.CratesLoadingProblem.Move;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MoveParserTest {
    @Test
    void should_parse_move() {
        var move = MoveParser.parse("move 12 from 8 to 2");

        Assertions.assertEquals(new Move(12, 8, 2), move);
    }
}
