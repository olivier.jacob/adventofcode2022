package AdventOfCode2022.day5;

import AdventOfCode2022.day5.CratesLoadingProblem.Move;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Stack;

class CratesLoadingProblemTest {

    @Test
    void should_move_top_crate_to_given_stack() {
        ArrayList<Stack<Character>> stacks = new ArrayList<>(3);

        stacks.add(0, CratesLoadingProblem.stackWith('Z', 'N'));
        stacks.add(1, CratesLoadingProblem.stackWith('M', 'C', 'D'));
        stacks.add(2, CratesLoadingProblem.stackWith('P'));

        var problem = new CratesLoadingProblem(stacks);
        problem.applyMove(Move.of(1, 2, 1));

        Assertions.assertAll(
            () -> Assertions.assertEquals('D', stacks.get(0).peek()),
            () -> Assertions.assertEquals('C', stacks.get(1).peek()),
            () -> Assertions.assertEquals('P', stacks.get(2).peek())
        );
    }

    @Test
    void should_move_top_crate_items_to_given_stack() {
        ArrayList<Stack<Character>> stacks = new ArrayList<>(3);

        stacks.add(0, CratesLoadingProblem.stackWith('Z', 'N'));
        stacks.add(1, CratesLoadingProblem.stackWith('M', 'C', 'D', 'Y'));
        stacks.add(2, CratesLoadingProblem.stackWith('P'));

        var problem = new CratesLoadingProblem(stacks);
        problem.applyMove(Move.of(3, 2, 1));

        Assertions.assertAll(
            () -> Assertions.assertEquals('Y', stacks.get(0).peek()),
            () -> Assertions.assertEquals('M', stacks.get(1).peek()),
            () -> Assertions.assertEquals('P', stacks.get(2).peek())
        );
    }

    @Test
    void should_apply_successive_moves() {
        ArrayList<Stack<Character>> stacks = new ArrayList<>(3);

        stacks.add(0, CratesLoadingProblem.stackWith('Z', 'N'));
        stacks.add(1, CratesLoadingProblem.stackWith('M', 'C', 'D'));
        stacks.add(2, CratesLoadingProblem.stackWith('P'));

        var problem = new CratesLoadingProblem(stacks);
        problem.applyMove(Move.of(1, 2, 1));
        problem.applyMove(Move.of(3, 1, 3));
        problem.applyMove(Move.of(2, 2, 1));
        problem.applyMove(Move.of(1, 1, 2));


        Assertions.assertEquals("MCD", problem.topStackItems());
    }
}
