package AdventOfCode2022.day6;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SignalStartDetectionProblemTest {

    @Test
    void should_find_four_different_consecutive_characters() {
        var problem = new SignalStartDetectionProblem();

        Assertions.assertFalse(problem.isStartMarker("mjqj"));
        Assertions.assertFalse(problem.isStartMarker("jqjp"));
        Assertions.assertFalse(problem.isStartMarker("qjpq"));
        Assertions.assertTrue(problem.isStartMarker("jpqm"));
    }

    @Test
    void should_detect_first_marker_occurence() {
        var problem = new SignalStartDetectionProblem();

        Assertions.assertEquals(7, problem.firstMarkerOccurenceIndex("mjqjpqmgbljsphdztnvjfqwrcgsmlb"));
        Assertions.assertEquals(5, problem.firstMarkerOccurenceIndex("bvwbjplbgvbhsrlpgdmjqwftvncz"));
        Assertions.assertEquals(6, problem.firstMarkerOccurenceIndex("nppdvjthqldpwncqszvftbrmjlhg"));
        Assertions.assertEquals(10, problem.firstMarkerOccurenceIndex("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"));
        Assertions.assertEquals(11, problem.firstMarkerOccurenceIndex("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"));
    }

    @Test
    void should_detect_message_marker_first_occurence() {
        var problem = new SignalStartDetectionProblem();

        Assertions.assertEquals(19, problem.firstMessageMarkerOccurence("mjqjpqmgbljsphdztnvjfqwrcgsmlb"));
        Assertions.assertEquals(23, problem.firstMessageMarkerOccurence("bvwbjplbgvbhsrlpgdmjqwftvncz"));
        Assertions.assertEquals(23, problem.firstMessageMarkerOccurence("nppdvjthqldpwncqszvftbrmjlhg"));
        Assertions.assertEquals(29, problem.firstMessageMarkerOccurence("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"));
        Assertions.assertEquals(26, problem.firstMessageMarkerOccurence("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"));
    }
}
