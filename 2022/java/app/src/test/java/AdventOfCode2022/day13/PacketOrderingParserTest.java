package AdventOfCode2022.day13;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class PacketOrderingParserTest {
    @Test
    void should_parse_single_list_line_to_packet() {
        String input = "[1,1,3,1,1]";

        PacketOrderingParser parser = new PacketOrderingParser();
        Packet packet = parser.parseLine(input);

        Assertions.assertThat(packet.toString()).isEqualTo("[1,1,3,1,1]");
    }

    @Test
    void should_parse_line_with_subpacket() {
        String input = "[[1],[2,3,4]]";

        PacketOrderingParser parser = new PacketOrderingParser();
        Packet packet = parser.parseLine(input);

        Assertions.assertThat(packet.toString()).isEqualTo("[[1],[2,3,4]]");
    }

    @Test
    void should_parse_line_with_deep_sub_packets() {
        String input = "[1,[2,[3,[4,[5,6,0]]]],8,9]";

        PacketOrderingParser parser = new PacketOrderingParser();
        Packet packet = parser.parseLine(input);

        Assertions.assertThat(packet.toString()).isEqualTo("[1,[2,[3,[4,[5,6,0]]]],8,9]");
    }

    @Test
    void should_parse_line_with_empty_sub_packets() {
        String input = "[[[]]]";

        PacketOrderingParser parser = new PacketOrderingParser();
        Packet packet = parser.parseLine(input);

        Assertions.assertThat(packet.toString()).isEqualTo("[[[]]]");
    }

    @Test
    void should_parse_numbers_with_more_than_1_digit() {
        String input = "[[10],[[[0,2,9,5,10],[],5,[]],5,10],[],[2,[[2,5,1],4,[4,3]],2,1],[]]";

        PacketOrderingParser parser = new PacketOrderingParser();
        Packet packet = parser.parseLine(input);

        Assertions.assertThat(packet.toString()).isEqualTo("[[10],[[[0,2,9,5,10],[],5,[]],5,10],[],[2,[[2,5,1],4,[4,3]],2,1],[]]");
    }
}
