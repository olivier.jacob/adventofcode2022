package AdventOfCode2022.day13;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PacketCheckerTest {
    @Test
    void should_stop_when_left_digit_is_lower() {
        var pair = new PacketOrderingParser().parsePacketPair(1, "[1,1,3,1,1]", "[1,1,5,1,1]");

        Assertions.assertTrue(pair.isOrdered());
    }

    @Test
    void should_compare_with_sub_packets() {
        assertOrdered("[[1],[2,3,4]]", "[[1],4]", true);

        assertOrdered("[9]", "[[8,7,6]]", false);

        assertOrdered("[[4,4],4,4]", "[[4,4],4,4,4]", true);

        assertOrdered("[7,7,7,7]", "[7,7,7]", false);

        assertOrdered("[]", "[3]", true);

        assertOrdered("[[[]]]", "[[]]", false);

        assertOrdered("[1,[2,[3,[4,[5,6,7]]]],8,9]", "[1,[2,[3,[4,[5,6,0]]]],8,9]", false);
    }

    @Test
    void acceptance_tests() {
        assertOrdered(
            "[[],[1,[3,[0]]],[]]",
            "[[],[[0,[5,3,0,1,0],[3,0,5,7],10,[2,8,5,0]],10,[2,4,[1],[5,6,7],[]],[]]]",
            false
        );

        assertOrdered(
            "[[[10,9,9,[0,1],4],[[2,3,2,9],10,5,[],[]],[6],2,[8,2,6]],[7,[3,[3,3,4],[2],[2],[0]],[],1],[[6,3],[[3,1],[10,2,5,7]],3,8,[[7,2,7],6,9,[]]]]",
            "[[1],[9],[[[2],3,[]],8],[[[2,7,5,1,8],[1,1,2]],8,[1,1,1,6],0,[1,[5,8,2],7,[7,7,4,4],3]]]",
            false
        );

        assertOrdered(
            "[[9,[],[[10,10,1,4,10],[2,2,7,6,1],2,9]]]",
            "[[],[[[2,6,9],[],6,[8,8,4]],8,9,7],[9,[[9,10,5,10,6],[4,9,3],[9]],7],[[],[1,[5],[]],9],[[7,1,4,6,[2,1,9,7,8]],[[6,9],5],7]]",
            false
        );

        assertOrdered(
            "[[8,9,[9,[2,5,7,6,6],[7]]],[],[8]]",
            "[[10,[[],3,[10],[7,8],9],[10,3,[6,9,4],9,9],6],[[[],[8,2,3],9,[3,7,3,7]],[],[3,[5,8],10]],[],[[],4,[],[],2],[[[8,0,8,2],9,[3,5,6,8,6],4],1,[]]]",
            true
        );

        assertOrdered(
            "[[[1,[]],[4,[10]],0,[10,[6,2,0],[9,4],6],[]]]",
            "[[1],[[4],[0,4,3,[7,7,6,1,10],7],2,9,[3]],[[]],[],[7]]",
            false
        );

        assertOrdered(
            "[[[[]],[5],[[9,9,1,7,5],[5,10,5,2,8],9,[10,7,4,0],[]],[9,2,6]]]",
            "[[[],4],[3,9],[2,6,8,[[]],[]],[]]",
            false
        );

        assertOrdered(
            "[[[],10,10],[[[4,8,5,3,5],[10,10,9],2,5,[5,7,9]],3]]",
            "[[],[[[7,7,5,1,6],[10,4,5,10],[3,2],[],1],0,6,9],[4,[],0],[[8,9],[[0,5,7],4],2,[[5,8,8,9,10],3,[1,1],8,[8,2]]]]",
            false
        );
    }

    private void assertOrdered(String left, String right, boolean isOrdered) {
        var pair = new PacketOrderingParser().parsePacketPair(1, left, right);

        if (isOrdered) {
            Assertions.assertTrue(pair.isOrdered());
        } else {
            Assertions.assertFalse(pair.isOrdered());
        }
    }
}
