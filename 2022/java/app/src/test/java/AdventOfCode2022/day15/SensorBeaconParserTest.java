package AdventOfCode2022.day15;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class SensorBeaconParserTest {
    @Test
    void shoud_parse_sensors_positions() {
        var sensorLines = Arrays.stream("""
            Sensor at x=2, y=18: closest beacon is at x=-2, y=15
            Sensor at x=9, y=16: closest beacon is at x=10, y=16
            Sensor at x=13, y=2: closest beacon is at x=15, y=3
            """.split("\n")).toList();

        List<Sensor> sensors = SensorBeaconParser.parseLines(sensorLines);

        Assertions.assertThat(sensors).containsExactly(
            new Sensor(2, 18, new Beacon(-2, 15)),
            new Sensor(9, 16, new Beacon(10, 16)),
            new Sensor(13, 2, new Beacon(15, 3))
        );
    }
}
