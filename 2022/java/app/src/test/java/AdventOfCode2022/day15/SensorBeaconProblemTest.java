package AdventOfCode2022.day15;

import AdventOfCode2022.day15.SensorBeaconProblem.Device;
import AdventOfCode2022.day15.SensorBeaconProblem.Segment;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class SensorBeaconProblemTest {

    @Test
    void segment_overlapping() {
        var segment1 = new Segment(0, 100, 0);
        Assertions.assertThat(segment1.overlaps(new Segment(-10, 50, 0))).isTrue();
        Assertions.assertThat(segment1.overlaps(new Segment(50, 150, 0))).isTrue();
        Assertions.assertThat(segment1.overlaps(new Segment(50, 70, 0))).isTrue();
        Assertions.assertThat(segment1.overlaps(new Segment(-100, 150, 0))).isTrue();

        var segment2 = new Segment(8, 10, 16);
        Assertions.assertThat(segment2.overlaps(new Segment(-3, 7, 0))).isFalse();
    }

    @Test
    void should_determine_beaconless_slots() {
        var sensorLines = exampleSensorLines();

        var sensors = SensorBeaconParser.parseLines(sensorLines);

        var problem = new SensorBeaconProblem(10);
        problem.detectBeaconLessSpots(sensors);
        long beaconLessSpots = problem.countBeaconLessSpotsOnLine(10);

        Assertions.assertThat(beaconLessSpots).isEqualTo(26);
    }

    @Test
    void should_find_distress_beacon() {
        var sensorLines = exampleSensorLines();

        var sensors = SensorBeaconParser.parseLines(sensorLines);

        var problem = new SensorBeaconProblem();
        problem.detectBeaconLessSpots(sensors);
        Device distressBeacon = problem.findDistressBeacon(20);

        Assertions.assertThat(distressBeacon.x()).isEqualTo(14);
        Assertions.assertThat(distressBeacon.y()).isEqualTo(11);
    }

    private static List<String> exampleSensorLines() {
        return Arrays.stream("""
            Sensor at x=2, y=18: closest beacon is at x=-2, y=15
            Sensor at x=9, y=16: closest beacon is at x=10, y=16
            Sensor at x=13, y=2: closest beacon is at x=15, y=3
            Sensor at x=12, y=14: closest beacon is at x=10, y=16
            Sensor at x=10, y=20: closest beacon is at x=10, y=16
            Sensor at x=14, y=17: closest beacon is at x=10, y=16
            Sensor at x=8, y=7: closest beacon is at x=2, y=10
            Sensor at x=2, y=0: closest beacon is at x=2, y=10
            Sensor at x=0, y=11: closest beacon is at x=2, y=10
            Sensor at x=20, y=14: closest beacon is at x=25, y=17
            Sensor at x=17, y=20: closest beacon is at x=21, y=22
            Sensor at x=16, y=7: closest beacon is at x=15, y=3
            Sensor at x=14, y=3: closest beacon is at x=15, y=3
            Sensor at x=20, y=1: closest beacon is at x=15, y=3
            """.split("\n")).toList();
    }
}
