package AdventOfCode2022.day7;

import AdventOfCode2022.day7.OutputTokens.ChangeDirectory;
import AdventOfCode2022.day7.OutputTokens.Dir;
import AdventOfCode2022.day7.OutputTokens.EndToken;
import AdventOfCode2022.day7.OutputTokens.Filesize;
import AdventOfCode2022.day7.OutputTokens.ListContent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class TerminalOutputTokenizerTest {

    @Test
    void should_detect_end_of_content() {
        TerminalOutputTokenizer tokenizer = new TerminalOutputTokenizer(List.of());

        var nextToken = tokenizer.next();
        Assertions.assertInstanceOf(EndToken.class, nextToken);
        Assertions.assertTrue(nextToken.isEndToken());
    }

    @Test
    void should_detect_command_line() {
        TerminalOutputTokenizer tokenizer = new TerminalOutputTokenizer(
            List.of("$ cd /", "$ cd ctd")
        );

        assertNextIsChangeDirectoryWithTarget(tokenizer, "/");
        assertNextIsChangeDirectoryWithTarget(tokenizer, "ctd");
    }

    @Test
    void should_detect_ls() {
        TerminalOutputTokenizer tokenizer = new TerminalOutputTokenizer(
            List.of("$ ls")
        );

        assertNextIsLs(tokenizer);
    }

    @Test
    void should_detect_dir() {
        TerminalOutputTokenizer tokenizer = new TerminalOutputTokenizer(
            List.of("dir ctd")
        );

        assertNextIsDirWithName(tokenizer, "ctd");
    }

    @Test
    void should_detect_file_with_size() {
        TerminalOutputTokenizer tokenizer = new TerminalOutputTokenizer(
            List.of("80649 mwcj.pmh")
        );

        assertNextIsFileWithSize(tokenizer, "mwcj.pmh", 80649);
    }

    private static void assertNextIsChangeDirectoryWithTarget(TerminalOutputTokenizer tokenizer, String expectedTargetDir) {
        OutputToken nextToken = tokenizer.next();

        Assertions.assertInstanceOf(ChangeDirectory.class, nextToken);

        ChangeDirectory cd = (ChangeDirectory) nextToken;
        Assertions.assertEquals(expectedTargetDir, cd.targetDirectory());
    }

    private static void assertNextIsLs(TerminalOutputTokenizer tokenizer) {
        var nextToken = tokenizer.next();

        Assertions.assertInstanceOf(ListContent.class, nextToken);
    }

    private static void assertNextIsDirWithName(TerminalOutputTokenizer tokenizer, String expectedDirName) {
        OutputToken nextToken = tokenizer.next();

        Assertions.assertInstanceOf(Dir.class, nextToken);

        Dir dir = (Dir) nextToken;
        Assertions.assertEquals(expectedDirName, dir.name());
    }

    private static void assertNextIsFileWithSize(TerminalOutputTokenizer tokenizer, String expectedFilename, int expectedSize) {
        OutputToken nextToken = tokenizer.next();

        Assertions.assertInstanceOf(Filesize.class, nextToken);

        Filesize file = (Filesize) nextToken;
        Assertions.assertEquals(expectedFilename, file.name());
        Assertions.assertEquals(expectedSize, file.size());
    }
}
