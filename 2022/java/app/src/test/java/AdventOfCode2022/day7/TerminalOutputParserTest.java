package AdventOfCode2022.day7;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class TerminalOutputParserTest {
    @Test
    void should_parse_directory_with_files_only() {
        var parser = new TerminalOutputParser(
            new TerminalOutputTokenizer(
                List.of(
                    "$ cd d",
                    "$ ls",
                    "4060174 j",
                    "8033020 d.log",
                    "5626152 d.ext",
                    "7214296 k"
                )
            )
        );

        Directory directory = parser.parse();

        Assertions.assertEquals(4, directory.children().size());
    }

    @Test
    void should_parse_directory_hierarchy() {
        var parser = new TerminalOutputParser(
            new TerminalOutputTokenizer(
                List.of(
                    "$ cd d",
                    "$ ls",
                    "4060174 j",
                    "8033020 d.log",
                    "dir ctc",
                    "5626152 d.ext",
                    "7214296 k",
                    "$ cd ctc",
                    "5626 g.ext",
                    "dir tyd",
                    "72146 z",
                    "$ cd tyd",
                    "5626 p.ext",
                    "72146 m"
                )
            )
        );

        Directory directory = parser.parse();
        Assertions.assertEquals(5, directory.children().size());
    }

    @Test
    void should_parse_directory_hierarchy_with_cd_up() {
        var parser = new TerminalOutputParser(
            new TerminalOutputTokenizer(
                List.of(
                    "$ cd d",
                    "$ ls",
                    "4060174 j",
                    "dir ctc",
                    "dir tyd",
                    "$ cd ctc",
                    "72146 z",
                    "$ cd ..",
                    "$ cd tyd",
                    "72146 m"
                )
            )
        );

        Directory directory = parser.parse();
        Assertions.assertEquals(3, directory.children().size());
    }

    @Test
    void should_parse_given_example() {
        var parser = new TerminalOutputParser(
            new TerminalOutputTokenizer(
                List.of(
                    "$ cd /",
                    "$ ls",
                    "dir a",
                    "14848514 b.txt",
                    "8504156 c.dat",
                    "dir d",
                    "$ cd a",
                    "$ ls",
                    "dir e",
                    "29116 f",
                    "2557 g",
                    "62596 h.lst",
                    "$ cd e",
                    "$ ls",
                    "584 i",
                    "$ cd ..",
                    "$ cd ..",
                    "$ cd d",
                    "$ ls",
                    "4060174 j",
                    "8033020 d.log",
                    "5626152 d.ext",
                    "7214296 k"
                )
            )
        );

        Directory directory = parser.parse();
        Assertions.assertEquals(4, directory.children().size());
    }
}
