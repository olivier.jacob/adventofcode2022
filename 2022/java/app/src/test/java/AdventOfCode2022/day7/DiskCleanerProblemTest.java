package AdventOfCode2022.day7;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class DiskCleanerProblemTest {
    @Test
    void should_compute_size_for_example() {
        var parser = new TerminalOutputParser(
            new TerminalOutputTokenizer(
                List.of(
                    "$ cd /",
                    "$ ls",
                    "dir a",
                    "14848514 b.txt",
                    "8504156 c.dat",
                    "dir d",
                    "$ cd a",
                    "$ ls",
                    "dir e",
                    "29116 f",
                    "2557 g",
                    "62596 h.lst",
                    "$ cd e",
                    "$ ls",
                    "584 i",
                    "$ cd ..",
                    "$ cd ..",
                    "$ cd d",
                    "$ ls",
                    "4060174 j",
                    "8033020 d.log",
                    "5626152 d.ext",
                    "7214296 k"
                )
            )
        );

        Directory rootDir = parser.parse();

        long totalSize = new SizeCalculator(rootDir).totalSize();

        Assertions.assertEquals(95437, totalSize);
    }

    @Test
    void should_determine_dir_to_delete() {
        var parser = new TerminalOutputParser(
            new TerminalOutputTokenizer(
                List.of(
                    "$ cd /",
                    "$ ls",
                    "dir a",
                    "14848514 b.txt",
                    "8504156 c.dat",
                    "dir d",
                    "$ cd a",
                    "$ ls",
                    "dir e",
                    "29116 f",
                    "2557 g",
                    "62596 h.lst",
                    "$ cd e",
                    "$ ls",
                    "584 i",
                    "$ cd ..",
                    "$ cd ..",
                    "$ cd d",
                    "$ ls",
                    "4060174 j",
                    "8033020 d.log",
                    "5626152 d.ext",
                    "7214296 k"
                )
            )
        );

        Directory rootDir = parser.parse();

        var directory = new SizeCalculator(rootDir).smallestWithSizeBiggerThan(21618835l);

        Assertions.assertEquals("d", directory.name());
    }
}
