package AdventOfCode2022.day9;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PositionTest {
    @Test
    void should_compute_distance_between_two_overlapping_positions() {
        assertAreAdjacent(
            true,
            new Position(0, 0),
            new Position(0, 0)
        );
    }

    @Test
    void should_compute_distance_between_two_linear_positions() {
        assertAreAdjacent(
            false,
            new Position(2, 0),
            new Position(0, 0)
        );
        assertAreAdjacent(
            false,
            new Position(0, 0),
            new Position(2, 0)
        );
        assertAreAdjacent(
            false,
            new Position(0, 2),
            new Position(0, 0)
        );
        assertAreAdjacent(
            false,
            new Position(0, 0),
            new Position(0, 2)
        );
        assertAreAdjacent(
            false,
            new Position(-2, 0),
            new Position(0, 0)
        );
        assertAreAdjacent(
            false,
            new Position(0, 0),
            new Position(-2, 0)
        );
    }

    @Test
    void should_compute_distance_between_two_diagonally_separated_positions() {
        assertAreAdjacent(
            true,
            new Position(1, 1),
            new Position(0, 0)
        );
        assertAreAdjacent(
            true,
            new Position(0, 0),
            new Position(1, 1)
        );
        assertAreAdjacent(
            false,
            new Position(0, 0),
            new Position(5, 5)
        );
        assertAreAdjacent(
            false,
            new Position(-1, -2),
            new Position(2, 2)
        );
        assertAreAdjacent(
            false,
            new Position(4, 2),
            new Position(3, 0)
        );
    }

    @Test
    void should_change_with_direction() {
        var pos = new Position(0, 0);

        Assertions.assertEquals(
            new Position(1, 0),
            pos.right()
        );
        Assertions.assertEquals(
            new Position(-1, 0),
            pos.left()
        );
        Assertions.assertEquals(
            new Position(0, 1),
            pos.up()
        );
        Assertions.assertEquals(
            new Position(0, -1),
            pos.down()
        );
    }

    private static void assertAreAdjacent(boolean adjacent, Position head, Position tail) {
        Assertions.assertEquals(adjacent, tail.isAdjacentTo(head));
    }
}
