package AdventOfCode2022.day9;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class RopeProblemTest {
    @Test
    void should_apply_move_right() {
        VisitedPositionsObserver observer = new VisitedPositionsObserver();

        var rope = new Rope(
            List.of(
                new Knot(new Position(0, 0)),
                new Knot(new Position(0, 0), observer)
            )
        );


        rope.apply(new Move("R", 4));

        Assertions.assertEquals(
            new Position(4, 0),
            rope.knots().get(0).position()
        );
        Assertions.assertEquals(
            new Position(3, 0),
            rope.knots().get(1).position()
        );

        org.assertj.core.api.Assertions.assertThat(observer.getVisitedPositions())
            .containsExactlyInAnyOrder(
                new Position(0, 0),
                new Position(1, 0),
                new Position(2, 0),
                new Position(3, 0)
            );
    }

    @Test
    void should_apply_move_left() {
        VisitedPositionsObserver observer = new VisitedPositionsObserver();

        var rope = new Rope(
            List.of(
                new Knot(new Position(0, 0)),
                new Knot(new Position(0, 0), observer)
            )
        );

        rope.apply(new Move("L", 3));

        Assertions.assertEquals(
            new Position(-3, 0),
            rope.knots().get(0).position()
        );
        Assertions.assertEquals(
            new Position(-2, 0),
            rope.knots().get(1).position()
        );

        org.assertj.core.api.Assertions.assertThat(observer.getVisitedPositions())
            .containsExactlyInAnyOrder(
                new Position(0, 0),
                new Position(-1, 0),
                new Position(-2, 0)
            );
    }

    @Test
    void should_apply_move_up() {
        VisitedPositionsObserver observer = new VisitedPositionsObserver();

        var rope = new Rope(
            List.of(
                new Knot(new Position(0, 0)),
                new Knot(new Position(0, 0), observer)
            )
        );

        rope.apply(new Move("U", 3));

        Assertions.assertEquals(
            new Position(0, 3),
            rope.knots().get(0).position()
        );
        Assertions.assertEquals(
            new Position(0, 2),
            rope.knots().get(1).position()
        );

        org.assertj.core.api.Assertions.assertThat(observer.getVisitedPositions())
            .containsExactlyInAnyOrder(
                new Position(0, 0),
                new Position(0, 1),
                new Position(0, 2)
            );
    }

    @Test
    void should_apply_move_down() {
        VisitedPositionsObserver observer = new VisitedPositionsObserver();

        var rope = new Rope(
            List.of(
                new Knot(new Position(0, 0)),
                new Knot(new Position(0, 0), observer)
            )
        );

        rope.apply(new Move("D", 3));

        Assertions.assertEquals(
            new Position(0, -3),
            rope.knots().get(0).position()
        );
        Assertions.assertEquals(
            new Position(0, -2),
            rope.knots().get(1).position()
        );

        org.assertj.core.api.Assertions.assertThat(observer.getVisitedPositions())
            .containsExactlyInAnyOrder(
                new Position(0, 0),
                new Position(0, -1),
                new Position(0, -2)
            );
    }

    @Test
    void should_apply_moves_one_after_the_other() {
        var moves = List.of(
            new Move("R", 4),
            new Move("U", 4),
            new Move("L", 3),
            new Move("D", 1),
            new Move("R", 4),
            new Move("D", 1),
            new Move("L", 5),
            new Move("R", 2)
        );

        VisitedPositionsObserver observer = new VisitedPositionsObserver();

        var rope = new Rope(
            List.of(
                new Knot(new Position(0, 0)),
                new Knot(new Position(0, 0), observer)
            )
        );

        for (Move move : moves) {
            rope.apply(move);
        }

        Assertions.assertEquals(
            new Position(2, 2),
            rope.knots().get(0).position()
        );
        Assertions.assertEquals(
            new Position(1, 2),
            rope.knots().get(1).position()
        );

        org.assertj.core.api.Assertions.assertThat(observer.getVisitedPositions())
            .containsExactlyInAnyOrder(
                new Position(0, 0),
                new Position(1, 0),
                new Position(2, 0),
                new Position(3, 0),
                new Position(4, 1),
                new Position(4, 2),
                new Position(3, 2),
                new Position(2, 2),
                new Position(1, 2),
                new Position(4, 3),
                new Position(3, 3),
                new Position(3, 4),
                new Position(2, 4)
            );
    }

    @Test
    void should_handle_a_longer_rope() {
        var moves = List.of(
            new Move("R", 5),
            new Move("U", 8),
            new Move("L", 8),
            new Move("D", 3),
            new Move("R", 17),
            new Move("D", 10),
            new Move("L", 25),
            new Move("U", 20)
        );

        VisitedPositionsObserver observer = new VisitedPositionsObserver();

        var numberOfKnots = 10;
        var rope = Rope.withKnots(numberOfKnots);
        rope.getknot(9).observe(observer);

        for (Move move : moves) {
            rope.apply(move);
        }

        Assertions.assertEquals(36, observer.getVisitedPositions().size());
    }
}
