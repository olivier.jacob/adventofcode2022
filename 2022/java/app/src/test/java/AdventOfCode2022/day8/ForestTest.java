package AdventOfCode2022.day8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class ForestTest {

    @Test
    void should_parse_input_into_forest() {
        var forest = forestFromInput(
            """
            111
            141
            111
            """
        );

        Assertions.assertEquals(1, forest.getTree(0, 0));
        Assertions.assertEquals(1, forest.getTree(0, 2));
        Assertions.assertEquals(1, forest.getTree(2, 0));
        Assertions.assertEquals(1, forest.getTree(2, 2));
        Assertions.assertEquals(4, forest.getTree(1, 1));
    }

    @Test
    void should_consider_an_outermost_tree_as_visible() {
        var forest = forestFromInput(
            """
            111
            141
            111
            """
        );

        Assertions.assertTrue(forest.isTreeVisible(0, 0));
        Assertions.assertTrue(forest.isTreeVisible(0, 1));
        Assertions.assertTrue(forest.isTreeVisible(0, 2));
        Assertions.assertTrue(forest.isTreeVisible(1, 0));
        Assertions.assertTrue(forest.isTreeVisible(1, 2));
        Assertions.assertTrue(forest.isTreeVisible(2, 0));
        Assertions.assertTrue(forest.isTreeVisible(2, 1));
        Assertions.assertTrue(forest.isTreeVisible(2, 2));
    }

    @Test
    void should_consider_tree_visible_when_all_left_trees_smaller() {
        var forest = forestFromInput(
            """
            3333
            3413
            3113
            3333
            """
        );

        Assertions.assertTrue(forest.isTreeVisible(1, 1));
        Assertions.assertFalse(forest.isTreeVisible(1, 2));
    }

    @Test
    void should_consider_tree_visible_when_all_right_trees_smaller() {
        var forest = forestFromInput(
            """
            3333
            3411
            3121
            3333
            """
        );

        Assertions.assertTrue(forest.isTreeVisible(1, 1));
        Assertions.assertFalse(forest.isTreeVisible(1, 2));
        Assertions.assertTrue(forest.isTreeVisible(2, 2));
    }

    @Test
    void should_count_visible_trees() {
        var forest = forestFromInput(
            """
                30373
                25512
                65332
                33549
                35390
                """
        );

        Assertions.assertEquals(21, forest.countVisibleTrees());
    }
    
    @Test
    void should_compute_scenic_score_for_tree() {
        var forest = forestFromInput(
            """
                30373
                25512
                65332
                33549
                35390
                """
        );

        // Tree on the edge has no scenic view
        Assertions.assertEquals(0, forest.scenicScore(0, 2));
        Assertions.assertEquals(0, forest.scenicScore(4, 1));
        Assertions.assertEquals(0, forest.scenicScore(2, 0));
        Assertions.assertEquals(0, forest.scenicScore(3, 4));

        Assertions.assertEquals(4, forest.scenicScore(1, 2));
        Assertions.assertEquals(8, forest.scenicScore(3, 2));
    }

    @Test
    void should_compute_highest_scenic_score_for_tree() {
        var forest = forestFromInput(
            """
                30373
                25512
                65332
                33549
                35390
                """
        );


        Assertions.assertEquals(8, forest.highestScenicScore());
    }

    private static Forest forestFromInput(String input) {
        return new Forest(Arrays.stream(input.split("\\n")).toList());
    }

}
