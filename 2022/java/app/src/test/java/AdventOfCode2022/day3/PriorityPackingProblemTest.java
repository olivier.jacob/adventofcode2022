package AdventOfCode2022.day3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class PriorityPackingProblemTest {
    @Test
    void should_find_common_item_in_rucksack() {
        assertCommonItemInRucksack("vJrwpWtwJgWrhcsFMMfFFhFp", 'p');
        assertCommonItemInRucksack("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL", 'L');
        assertCommonItemInRucksack("PmmdzqPrVvPwwTWBwg", 'P');
        assertCommonItemInRucksack("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn", 'v');
        assertCommonItemInRucksack("ttgJtRGJQctTZtZT", 't');
        assertCommonItemInRucksack("CrZsJsPPZsGzwwsLwLmpwMDw", 's');
        assertCommonItemInRucksack("ZmcgBBZhZMsnqnCPjpHPjLHp", 'n');
        assertCommonItemInRucksack("TLlfQpffQvvzhtNqztRFtzcm", 'z');
    }

    @Test
    void should_correctly_assign_priority() {
        assertPriority('a', 1);
        assertPriority('b', 2);
        assertPriority('z', 26);
        assertPriority('A', 27);
        assertPriority('B', 28);
        assertPriority('Z', 52);
        assertPriority('0', 0);
    }

    @Test
    void should_give_priority_of_common_item_in_rucksack() {
        assertCommonItemPriority("vJrwpWtwJgWrhcsFMMfFFhFp", 16);
        assertCommonItemPriority("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL", 38);
        assertCommonItemPriority("PmmdzqPrVvPwwTWBwg", 42);
        assertCommonItemPriority("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn", 22);
        assertCommonItemPriority("ttgJtRGJQctTZtZT", 20);
        assertCommonItemPriority("CrZsJsPPZsGzwwsLwLmpwMDw", 19);
        assertCommonItemPriority("ZmcgBBZhZMsnqnCPjpHPjLHp", 14);
        assertCommonItemPriority("TLlfQpffQvvzhtNqztRFtzcm", 26);
    }

    @Test
    void should_find_common_item_between_three_rucksacks() {
        assertCommonItemBetweenRucksacks(
            List.of("vJrwpWtwJgWrhcsFMMfFFhFp", "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL", "PmmdzqPrVvPwwTWBwg"),
            'r'
        );
        assertCommonItemBetweenRucksacks(
            List.of("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn", "ttgJtRGJQctTZtZT", "CrZsJsPPZsGzwwsLwLmpwMDw"),
            'Z'
        );
    }

    @Test
    void should_compute_sum_of_priorities_for_groups() {
        var groupsPriorities = new PriorityPackingProblem().scoreRucksacksByGroupsOf(
            List.of(
                "vJrwpWtwJgWrhcsFMMfFFhFp",
                "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
                "PmmdzqPrVvPwwTWBwg",
                "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
                "ttgJtRGJQctTZtZT",
                "CrZsJsPPZsGzwwsLwLmpwMDw"
            ), 3
        );
        Assertions.assertEquals(70, groupsPriorities);
    }

    private void assertCommonItemBetweenRucksacks(List<String> rucksacks, char expectedCommon) {
        Assertions.assertEquals(
            expectedCommon,
            new PriorityPackingProblem().commonInAll(rucksacks)
        );
    }

    private static void assertCommonItemInRucksack(String rucksack, char expectedCommonItem) {
        char commonItem = new PriorityPackingProblem().findCommonItem(rucksack);
        Assertions.assertEquals(expectedCommonItem, commonItem);
    }

    private void assertPriority(char item, int expectedPriority) {
        int priority = new PriorityPackingProblem().itemPriority(item);
        Assertions.assertEquals(expectedPriority, priority);
    }

    private void assertCommonItemPriority(String rucksack, int expectedCommonItemPriority) {
        int priority = new PriorityPackingProblem().commonItemPriority(rucksack);
        Assertions.assertEquals(expectedCommonItemPriority, priority);
    }
}
