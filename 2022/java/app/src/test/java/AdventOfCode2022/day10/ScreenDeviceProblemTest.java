package AdventOfCode2022.day10;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Observer;

class ScreenDeviceProblemTest {

    @Test
    void should_parse_instructions() {
        var parser = new InstructionParser();
        var instructions = parser.parse(
            List.of(
                "noop",
                "addx 3",
                "addx -5"
            )
        );

        Assertions.assertThat(instructions)
            .containsExactly(
                new Noop(),
                new AddX(3),
                new AddX(-5)
            );
    }

    @Test
    void should_cycle_and_execute_single_cycle_instruction() {
        List<Instruction> instructions = List.of(new Noop());

        CPU cpu = new CPU(instructions);
        cpu.tick();

        Assertions.assertThat(cpu.getRegisterX().get()).isEqualTo(1);
    }

    @Test
    void should_cycle_and_execute_multi_cycles_instruction() {
        List<Instruction> instructions = List.of(new AddX(3));

        CPU cpu = new CPU(instructions);
        cpu.tick();
        Assertions.assertThat(cpu.getRegisterX().get()).isEqualTo(1);
        cpu.tick();
        Assertions.assertThat(cpu.getRegisterX().get()).isEqualTo(1);
        cpu.tick();
        Assertions.assertThat(cpu.getRegisterX().get()).isEqualTo(4);
    }

    @Test
    void should_execute_several_instructions_sequentially() {
        var instructions = List.of(
            new Noop(),
            new AddX(3),
            new AddX(-5)
        );

        var cpu = new CPU(instructions);

        var expectedX = new int[]{1, 1, 1, 4, 4, 4, -1};

        for (int cycle = 1; cpu.hasNextInstruction(); cycle++) {
            cpu.tick();
            Assertions.assertThat(cpu.getRegisterX().get()).isEqualTo(expectedX[cycle - 1]);
        }
    }

    @Test
    void should_compute_signal_strength_at_regular_interval() {
        var lines = List.of(
            "addx 15", "addx -11", "addx 6", "addx -3", "addx 5",
            "addx -1", "addx -8", "addx 13", "addx 4", "noop",
            "addx -1", "addx 5", "addx -1", "addx 5", "addx -1",
            "addx 5", "addx -1", "addx 5", "addx -1", "addx -35",
            "addx 1", "addx 24", "addx -19", "addx 1", "addx 16",
            "addx -11", "noop"
        );
        var instructions = new InstructionParser().parse(lines);

        var cpu = new CPU(instructions);
        var signalStrenghObserver = new SignalStrenghObserver();
        cpu.registerObserver(signalStrenghObserver);

        while (cpu.hasNextInstruction()) {
            cpu.tick();
        }

        Assertions.assertThat(signalStrenghObserver.totalStrenghts()).isEqualTo(420);
    }
}
