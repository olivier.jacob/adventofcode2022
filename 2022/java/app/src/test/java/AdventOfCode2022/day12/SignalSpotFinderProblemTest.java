package AdventOfCode2022.day12;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

class SignalSpotFinderProblemTest {
    @Test
    void should_parse_and_find_start_and_end_cells() {
        List<String> exampleInput = Arrays.stream("""
            Sabqponm
            abcryxxl
            accszExk
            acctuvwj
            abdefghi
            """.split("\n")).toList();

        var problem = new SignalSpotFinderProblem();
        var heightMap = problem.parse(exampleInput);

        var startCell = heightMap.getStartCell();
        Assertions.assertThat(startCell).isEqualTo(new Cell(0, 0));
        Assertions.assertThat(heightMap.getHeightAt(startCell.row(), startCell.col())).isEqualTo(0);

        var endCell = heightMap.getEndCell();
        Assertions.assertThat(endCell).isEqualTo(new Cell(2, 5));
        Assertions.assertThat(heightMap.getHeightAt(endCell.row(), endCell.col())).isEqualTo(25);
    }

    @Test
    void should_find_shortest_path_to_highest_spot() {
        List<String> exampleInput = Arrays.stream("""
            Sabqponm
            abcryxxl
            accszExk
            acctuvwj
            abdefghi
            """.split("\n")).toList();

        var problem = new SignalSpotFinderProblem();
        var heightMap = problem.parse(exampleInput);

        List<Cell> path = problem.shortestPath(heightMap);

        Assertions.assertThat(path).hasSize(32);
    }

    @Test
    void should_find_most_scenic_path_to_highest_spot() {
        List<String> exampleInput = Arrays.stream("""
            Sabqponm
            abcryxxl
            accszExk
            acctuvwj
            abdefghi
            """.split("\n")).toList();

        var problem = new SignalSpotFinderProblem();
        var heightMap = problem.parse(exampleInput);

        List<Cell> path = problem.mostScenicPath(heightMap);

        Assertions.assertThat(path).hasSize(30);
    }
}
