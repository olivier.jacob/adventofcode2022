import re

stacks = []

input = open(0)
for line in input:
    if line == '\n':
        break;
    
    stacks.append(list(line[1::4]))
  
# Removing last element because we parsed the stack index line
stacks.pop()

# Now need to rotate the stacks, because we parsed them horizontally
# Zipping all rows between themselves makes them colums...
# Then we reverse the order so that the last element is the top element
stacks = [list("".join(col).strip()[::-1 ]) for col in zip(*stacks)]

# Now parse the move instructions
for line in input:
    qty, fromStack, toStack = map(int, re.findall("\\d+", line))

    stacks[toStack - 1].extend(stacks[fromStack - 1][-qty:][::-1])
    stacks[fromStack - 1] = stacks[fromStack - 1][:-qty]

# Take the top element of each stack
print("".join([ s[-1] for s in stacks ]))