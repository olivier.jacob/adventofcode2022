t = 0

for line in open(0):
    mid = len(line) // 2
    left = line[:mid]
    right = line[mid:]

    common, = set(left) & set(right)

    if common >= "a":
        t += ord(common) - ord('a') + 1
    else:
        t += ord(common) - ord('A') + 27

print(t)
