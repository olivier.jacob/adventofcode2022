t = 0

while True:
    try:
        x = input()
        y = input()
        z = input()
    except:
        break
    
    common, = set(x) & set(y) & set(z)

    if common >= "a":
        t += ord(common) - ord('a') + 1
    else:
        t += ord(common) - ord('A') + 27

print(t)
