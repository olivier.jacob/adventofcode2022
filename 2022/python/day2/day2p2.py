
score = 0

for line in open(0).read().splitlines():
    p1,p2 = line.split()

    p1 = ord(p1) - ord('A')
    p2 = ord(p2) - ord('X')

    if p2 == 0:
        #lost
        score += (p1 - 1) % 3 + 1
    elif p2 == 1:
        # draw
        score += 3
        score += p1 + 1
    else:
        #win
        score += 6
        score += (p1 + 1) % 3 + 1

print(score)
