
score = 0

for line in open(0).read().splitlines():
    p1,p2 = line.split()

    p1 = ord(p1) - ord('A')
    p2 = ord(p2) - ord('X')

    if (p1 == p2):
        score += 3
    elif ((p2 - p1) % 3 == 1):
        score += 6

    score += p2 + 1

print(score)
