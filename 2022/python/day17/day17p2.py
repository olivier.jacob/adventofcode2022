
rocks = [
    [0, 1, 2, 3],
    [1, 1j, 1+1j, 2+1j, 1+2j],
    [0, 1, 2, 2+1j, 2+2j],
    [0, 1j, 2j, 3j],
    [0, 1, 1j, 1+1j]
]

jets = [1 if c == ">" else -1 for c in input()]
solid = { x - 1j for x in range(7) }

def summarize():
    o = [-20] * 7
    
    for x in solid:
        r = int(x.real)
        i = int(x.imag)
        o[r] = max(o[r], i)
    
    top = max(o)
    return tuple(x - top for x in o)

ROCK_CNT = 1000000000000

seen = {}
height = 0
rockCount = 0
rockIndex = 0
currentRock = { x + 2 + (height + 3) * 1j for x in rocks[rockIndex] }

while rockCount < ROCK_CNT:
    for jetIndex, jet in enumerate(jets):
        movedRock = { x + jet for x in currentRock }

        if all(0 <= x.real < 7 for x in movedRock) and not (movedRock & solid):
            currentRock = movedRock

        movedRock = { x - 1j for x in currentRock }

        if (movedRock & solid):
            solid |= currentRock
            rockCount += 1
            height = max(x.imag for x in solid) + 1
            
            if (rockCount >= ROCK_CNT):
                break

            rockIndex = (rockIndex + 1) % len(rocks)
            currentRock = { x + 2 + (height + 3) * 1j for x in rocks[rockIndex] }

            # Trying to find the pattern
            key = (jetIndex, rockIndex, summarize())
            if key in seen:
                lastRockCount, lastHeight = seen[key]
                remainingRocks = ROCK_CNT - rockCount
                rep = remainingRocks // (rockCount - lastRockCount)
                offset = rep * (height - lastHeight)
                rockCount += rep * (rockCount - lastRockCount)
                seen = {}

            seen[key] = (rockCount, height)
        else:
            currentRock = movedRock

print(int(height + offset))