
rocks = [
    [0, 1, 2, 3],
    [1, 1j, 1+1j, 2+1j, 1+2j],
    [0, 1, 2, 2+1j, 2+2j],
    [0, 1j, 2j, 3j],
    [0, 1, 1j, 1+1j]
]

jets = [1 if c == ">" else -1 for c in input()]
solid = { x - 1j for x in range(7) }
height = 0

rockCount = 0

rockIndex = 0
currentRock = { x + 2 + (height + 3) * 1j for x in rocks[rockIndex] }

ROCK_CNT = 2022

while rockCount < ROCK_CNT:
    for jet in jets:
        movedRock = { x + jet for x in currentRock }

        if all(0 <= x.real < 7 for x in movedRock) and not (movedRock & solid):
            currentRock = movedRock

        movedRock = { x - 1j for x in currentRock }

        if (movedRock & solid):
            solid |= currentRock
            rockCount += 1
            height = max(x.imag for x in solid) + 1
            
            if (rockCount >= ROCK_CNT):
                break

            rockIndex = (rockIndex + 1) % len(rocks)
            currentRock = { x + 2 + (height + 3) * 1j for x in rocks[rockIndex] }
        else:
            currentRock = movedRock

print(int(height))