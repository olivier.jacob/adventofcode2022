import re

monkeys = {}

def isOnPath(m):
    if (m == 'humn'):
        return True
    elif monkeys[m].val is not None:
        return False
    else:
        return isOnPath(monkeys[m].left) or isOnPath(monkeys[m].right) 

class Monkey:
    val = None
    op = None
    left = None
    right = None

    def yell(self):
        if self.val is not None:
            return self.val
        else:
            l = monkeys[self.left].yell()
            r = monkeys[self.right].yell()
            if self.op == '+':
                return l + r
            elif self.op == '-':
                return l - r
            elif self.op == '*':
                return l * r
            elif self.op == '/':
                return l // r

    def findValue(self, val):
        if self.val is None and self.left is None and self.right is None:
            return val
        elif isOnPath(self.left):
            r = monkeys[self.right].yell()
            if self.op == '+':
                return monkeys[self.left].findValue(val - r)
            elif self.op == '-':
                return monkeys[self.left].findValue(val + r)
            elif self.op == '*':
                return monkeys[self.left].findValue(val // r)
            elif self.op == '/':
                return monkeys[self.left].findValue(val * r)
        else:
            l = monkeys[self.left].yell()
            if self.op == '+':
                return monkeys[self.right].findValue(val - l)
            elif self.op == '-':
                return monkeys[self.right].findValue(l - val)
            elif self.op == '*':
                return monkeys[self.right].findValue(val // l)
            elif self.op == '/':
                return monkeys[self.right].findValue(l // val * r)

for line in open(0).read().splitlines():
    monkey, op = line.split(':')
    
    if monkey == 'humn':
        m = Monkey()
        monkeys[monkey] = m
        continue

    monkeyDef = op.strip()
    if '0' <= monkeyDef[0] <= '9':
        m = Monkey()
        m.val = int(monkeyDef)
        monkeys[monkey] = m
    else:
        r = re.search('(\w+) ([+-/*]) (\w+)', monkeyDef)
        m = Monkey()
        m.op = r.group(2)
        m.left = r.group(1) 
        m.right = r.group(3)
        monkeys[monkey] = m


humanYell = 0
if isOnPath(monkeys['root'].left):
    humanYell = monkeys[monkeys['root'].left].findValue(monkeys[monkeys['root'].right].yell())
else:
    humanYell = monkeys[monkeys['root'].right].findValue(monkeys[monkeys['root'].left].yell())

print(humanYell)