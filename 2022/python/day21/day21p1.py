import re

monkeys = {}

class Monkey:
    val = None
    op = None
    left = None
    right = None

    def yell(self):
        if self.val is not None:
            return self.val
        else:
            l = monkeys[self.left].yell()
            r = monkeys[self.right].yell()
            if self.op == '+':
                return l + r
            elif self.op == '-':
                return l - r
            elif self.op == '*':
                return l * r
            elif self.op == '/':
                return l // r 


for line in open(0).read().splitlines():
    monkey, monkeyDef = line.split(': ')
    
    if monkeyDef.isdigit():
        m = Monkey()
        m.val = int(monkeyDef)
        monkeys[monkey] = m
    else:
        left, op, right = monkeyDef.split()
        m = Monkey()
        m.op = op
        m.left = left 
        m.right = right
        monkeys[monkey] = m

print(monkeys['root'].yell())