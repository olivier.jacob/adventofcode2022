

cubes = []

directions = [
    (0, 0, 1),
    (0, 0, -1),
    (0, 1, 0),
    (0, -1, 0),
    (1, 0, 0),
    (-1, 0, 0)
]

for line in open(0):
    x, y, z = map(int, line.split(','))
    cubes.append((x, y, z))

t = 0

for x, y, z in cubes:
    for dirX, dirY, dirZ in directions:
        movedCube = (x + dirX, y + dirY, z + dirZ)

        if (movedCube not in cubes):
            t += 1

print(t)
