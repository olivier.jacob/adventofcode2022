from collections import deque

cubes = []
faces = set()

directions = [
    (0, 0, 1),
    (0, 0, -1),
    (0, 1, 0),
    (0, -1, 0),
    (1, 0, 0),
    (-1, 0, 0)
]

minX = minY = minZ = float("inf")
maxX = maxY = maxZ = -float("inf")

for line in open(0):
    x, y, z = map(int, line.split(','))
    cubes.append((x, y, z))

    minX = min(minX, x)
    minY = min(minY, y)
    minZ = min(minZ, z)
    maxX = max(maxX, x)
    maxY = max(maxY, y)
    maxZ = max(maxZ, z)

minX -= 1
minY -= 1
minZ -= 1

maxX += 1
maxY += 1
maxZ += 1

in_bounds = (lambda x,y,z: minX <= x <= maxX and minY <= y <= maxY and minZ <= z <= maxZ)

seen = set()
stack = deque([(minX, minY, minZ)])

while stack:
    (cx,cy,cz) = cube = stack.popleft()
        
    if not in_bounds(cx,cy,cz):
        continue

    if cube not in seen and cube not in cubes:
        seen.add(cube)

        for dirX, dirY, dirZ in directions:
            stack.append((cx + dirX, cy + dirY, cz + dirZ))

t = 0
for x, y, z in cubes:
    for dirX, dirY, dirZ in directions:
        if ((x + dirX, y + dirY, z + dirZ) in seen):
            t += 1 

print(t)
