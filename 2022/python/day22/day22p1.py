import re

moves = []
grid = []

for line in open(0):
    l = line[:-1]
    if l == '':
        continue
    elif '0' <= l[0] <= '9':
        # Movement instructions line
        for steps, facing in re.findall('(\d+)([RL]?)', line):
            moves.append((int(steps), facing))
    else:
        # Grid line
        grid.append(l)

# Grab the max length for a row and pad the shorter ones with spaces
maxWidth = max(map(len, grid))
grid = [ line + ' ' * (maxWidth - len(line)) for line in grid ]

currentRow = 0
currentCol = grid[currentRow].index('.')
# (row direction, column direction)
deltaRow = 0
deltaCol = 1

for steps, facing in moves:
    for _ in range(steps):
        newRow = currentRow
        newCol = currentCol

        # Move in the direction and skip spaces
        while True:
            newRow = (newRow + deltaRow) % len(grid)
            newCol = (newCol + deltaCol) % len(grid[0])
            if grid[newRow][newCol] != ' ':
                break

        if grid[newRow][newCol] == '#':
            break;

        currentRow = newRow
        currentCol = newCol
    
    # Rotate according to new facing
    if facing == 'L':
        deltaRow, deltaCol = -deltaCol, deltaRow
    elif facing == 'R':
        deltaRow, deltaCol = deltaCol, -deltaRow
    
# Facing score, right is 0, down is 1, left is 2, up is 3
if deltaRow == 0:
    if deltaCol == 1: # right
        facingScore = 0
    else: # left
        facingScore = 2
else:
    if deltaRow == 1: # down
        facingScore = 1
    else: # up 
        facingScore = 3

print(1000 * (currentRow + 1) + 4 * (currentCol + 1) + facingScore)