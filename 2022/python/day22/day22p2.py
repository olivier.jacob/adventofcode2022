import re

moves = []
grid = []

for line in open(0):
    l = line[:-1]
    if l == '':
        continue
    elif '0' <= l[0] <= '9':
        # Movement instructions line
        for steps, facing in re.findall('(\d+)([RL]?)', line):
            moves.append((int(steps), facing))
    else:
        # Grid line
        grid.append(l)

# Grab the max length for a row and pad the shorter ones with spaces
maxWidth = max(map(len, grid))
grid = [ line + ' ' * (maxWidth - len(line)) for line in grid ]

N = 50 # let N be the width of a face

# Direction vectors
V_RIGHT = (0, 1)
V_LEFT = (0, -1)
V_UP = (-1, 0)
V_DOWN = (1, 0)

# Starting position
currentRow = 0
currentCol = grid[currentRow].index('.')
deltaRow, deltaCol = V_RIGHT


for steps, facing in moves:
    for i in range(steps):
        newDeltaRow = deltaRow
        newDeltaCol = deltaCol
        newRow = currentRow
        newCol = currentCol

        # Move in the current direction
        newRow = (newRow + newDeltaRow)
        newCol = (newCol + newDeltaCol)

        #############
        ##  Face 1 ##
        #############

        if 0 <= newRow < N and newCol == N - 1 and newDeltaCol == -1: # Exiting left of face 1 onto face 4
            newRow = 3 * N - 1 - newRow
            newCol = 0
            newDeltaRow, newDeltaCol = V_RIGHT

        # No special case here
        #elif 0 <= newRow < N and newCol == 2 * N and newDeltaCol == 1: # Exiting right of face 1 onto face 2
            # newRow left unchanged
            # newCol left unchanged
        #    newDeltaRow, newDeltaCol = V_RIGHT

        elif newRow < 0 and N <= newCol < 2 * N and newDeltaRow == -1: # Exiting top of face 1 onto face 6
            newRow = newCol + 2 * N
            newCol = 0
            newDeltaRow, newDeltaCol = V_RIGHT

        # No special case here
        #elif newRow == N and N <= newCol < 2 * N and newDeltaRow == 1: # Exiting bottom of face 1 onto face 3
            # newRow left unchanged
            # newCol left unchanged
        #    newDeltaRow, newDeltaCol = V_DOWN

        #############
        ##  Face 2 ##
        #############

        # No special case here
        #elif 0 <= newRow < N and newCol == 2 * N - 1 and newDeltaCol == -1: # Exiting left of face 2 onto face 1
            # newRow left unchanged
            # newCol left unchanged
        #    newDeltaRow, newDeltaCol = V_LEFT

        elif 0 <= newRow < N and newCol == 3 * N and newDeltaCol == 1: # Exiting right of face 2 onto face 5
            newRow = 3 * N - 1 - newRow 
            newCol = 2 * N - 1
            newDeltaRow, newDeltaCol = V_LEFT

        elif newRow == N and 2 * N <= newCol < 3 * N and newDeltaRow == 1: # Exiting bottom of face 2 onto face 3
            newRow = newCol - N
            newCol = 2 * N - 1
            newDeltaRow, newDeltaCol = V_LEFT

        elif newRow < 0 and 2 * N <= newCol < 3 * N and newDeltaRow == -1: # Exiting top of face 2 onto face 6
            newCol = newCol - 2 * N
            newRow = 4 * N - 1
            newDeltaRow, newDeltaCol = V_UP

        #############
        ##  Face 3 ##
        #############

        elif N <= newRow < 2 * N and newCol == N - 1 and newDeltaCol == -1: # Exiting left of face 3 onto face 4
            newCol = newRow - N
            newRow = 2 * N            
            newDeltaRow, newDeltaCol = V_DOWN

        elif N <= newRow < 2 * N and newCol == 2 * N and newDeltaCol == 1: # Exiting right of face 3 onto face 2
            newCol = newRow + N
            newRow = N - 1
            newDeltaRow, newDeltaCol = V_UP

        # No special case here
        #elif newRow == N - 1 and N <= newCol < 2 * N and newDeltaRow == -1: # Exiting top of face 3 onto face 1
            # newRow left unchanged
            # newCol left unchanged
        #    newDeltaRow, newDeltaCol = V_UP

        # No special case here
        #elif newRow == 2 * N and N <= newCol < 2 * N and newDeltaRow == 1: # Exiting bottom of face 3 onto face 5
            # newRow left unchanged
            # newCol left unchanged
        #    newDeltaRow, newDeltaCol = V_DOWN

        #############
        ##  Face 4 ##
        #############

        elif 2 * N <= newRow < 3 * N and newCol < 0 and newDeltaCol == -1: # Exiting left of face 4 onto face 1
            newRow = 3 * N - 1 - newRow
            newCol = N
            newDeltaRow, newDeltaCol = V_RIGHT

        # No special case here
        #elif 2 * N <= newRow < 3 * N and newCol == N and newDeltaCol == 1: # Exiting right of face 4 onto face 5
            # newRow left unchanged
            # newCol left unchanged
        #    newDeltaRow, newDeltaCol = V_RIGHT

        elif newRow == 2 * N - 1 and 0 <= newCol < N and newDeltaRow == -1: # Exiting top of face 4 onto face 3
            newRow = newCol + N
            newCol = N
            newDeltaRow, newDeltaCol = V_RIGHT

        # No special case here
        #elif newRow == 3 * N and 0 <= newCol < N and newDeltaRow == 1: # Exiting bottom of face 4 onto face 6
            # newRow left unchanged
            # newCol left unchanged
        #    newDeltaRow, newDeltaCol = V_DOWN

        #############
        ##  Face 5 ##
        #############

        # No special case here
        #elif 2 * N <= newRow < 3 * N and newCol == N - 1 and newDeltaCol == -1: # exiting left of face 5 onto face 4
            # newRow left unchanged
            # newCol left unchanged
        #    newDeltaRow, newDeltaCol = V_LEFT

        elif 2 * N <= newRow < 3 * N and newCol == 2 * N and newDeltaCol == 1: # exiting right of face 5 onto face 2
            newRow = 3 * N - 1 - newRow
            newCol = 3 * N - 1
            newDeltaRow, newDeltaCol = V_LEFT

        elif newRow == 3 * N and N <= newCol < 2 * N and newDeltaRow == 1: # exiting bottom of face 5 onto face 6
            newRow = newCol + 2 * N
            newCol = N - 1
            newDeltaRow, newDeltaCol = V_LEFT

        # No special case here
        #elif newRow == 2 * N - 1 and N <= newCol < 2 * N  and newDeltaCol == -1: # Exiting top of face 5 onto face 3
            # newRow left unchanged
            # newCol left unchanged
        #    newDeltaRow, newDeltaCol = V_UP

        #############
        ##  Face 6 ##
        #############

        elif 3 * N <= newRow < 4 * N and newCol < 0 and newDeltaCol == -1: # Exiting left of face 6 onto face 1
            newCol = newRow - 2 * N
            newRow = 0
            newDeltaRow, newDeltaCol = V_DOWN
        
        elif 3 * N <= newRow < 4 * N and newCol == N and newDeltaCol == 1: # Exiting right of face 6 onto face 5
            newCol = newRow - 2 * N
            newRow = 3 * N - 1
            newDeltaRow, newDeltaCol = V_UP

        # No special case here
        #elif newRow == 3 * N - 1 and 0 <= newCol <= N and newDeltaRow == -1: # Exiting top of face 6 onto face 4
            # newRow left unchanged
            # newCol left unchanged
        #    newDeltaRow, newDeltaCol = V_UP
        
        elif newRow == 4 * N and 0 <= newCol < N and newDeltaRow == 1: # Exiting bottom of face 6 onto face 2
            newCol = newCol + 2 * N
            newRow = 0
            newDeltaRow, newDeltaCol = V_DOWN
        

        if grid[newRow][newCol] == '#':
            break;

        currentRow = newRow
        currentCol = newCol
        deltaRow = newDeltaRow
        deltaCol = newDeltaCol
    
    # Rotate according to new facing
    if facing == 'L':
        deltaRow, deltaCol = -deltaCol, deltaRow
    elif facing == 'R':
        deltaRow, deltaCol = deltaCol, -deltaRow


# Facing score, right is 0, down is 1, left is 2, up is 3
if deltaRow == 0:
    if deltaCol == 1: # right
        facingScore = 0
    else: # left
        facingScore = 2
else:
    if deltaRow == 1: # down
        facingScore = 1
    else: # up 
        facingScore = 3

print(1000 * (currentRow + 1) + 4 * (currentCol + 1) + facingScore)