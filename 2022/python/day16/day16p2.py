import re

p = re.compile('Valve ([A-Z]+) has flow rate=([0-9]+); tunnel.? lead.? to valve.? (.*)')

flowRates = dict()
neighbours = dict()

for line in open(0):
    valve, rate, tunnels = p.findall(line.strip())[0]
    flowRates[valve] = int(rate)
    neighbours[valve] = tunnels.split(', ')

visitingCache = dict()

startingNode = 'AA'
allowedTime = 26

def dfs(opened, node, minutesLeft, elephants):
    if (minutesLeft == 0): 
        if elephants > 0:
            return dfs(opened, startingNode, allowedTime, elephants - 1)
        else: 
            return 0

    if (opened, node, minutesLeft, elephants) in visitingCache:
        return visitingCache[(opened, node, minutesLeft, elephants)]

    maxNeighbour = 0
    if node not in opened and flowRates[node] > 0:
        nextOpened = tuple(sorted(opened + (node,)))
        releasedPressure = (minutesLeft - 1) * flowRates[node]
        maxNeighbour = max(maxNeighbour, releasedPressure + dfs(nextOpened, node, minutesLeft - 1, elephants))

    for neighbour in neighbours[node]:   
        maxNeighbour = max(maxNeighbour, dfs(opened, neighbour, minutesLeft - 1, elephants))

    visitingCache[(opened, node, minutesLeft, elephants)] = maxNeighbour

    return maxNeighbour

score = dfs((), startingNode, allowedTime, 1)

print(score)
