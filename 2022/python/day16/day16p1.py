import re

p = re.compile('Valve ([A-Z]+) has flow rate=([0-9]+); tunnel.? lead.? to valve.? (.*)')

flowRates = dict()
neighbours = dict()

for line in open(0):
    valve, rate, tunnels = p.findall(line.strip())[0]
    flowRates[valve] = int(rate)
    neighbours[valve] = tunnels.split(', ')

visitingCache = dict()

def dfs(opened, node, minutesLeft):
    if (minutesLeft == 0): return 0

    if (opened, node, minutesLeft) in visitingCache:
        return visitingCache[(opened, node, minutesLeft)]

    maxNeighbour = 0
    if node not in opened and flowRates[node] > 0:
        nextOpened = tuple(sorted(opened + (node,)))
        releasedPressure = (minutesLeft - 1) * flowRates[node]
        maxNeighbour = max(maxNeighbour, releasedPressure + dfs(nextOpened, node, minutesLeft - 1))

    for neighbour in neighbours[node]:        
        maxNeighbour = max(
            maxNeighbour,
            dfs(opened, neighbour, minutesLeft - 1)
        )

    visitingCache[(opened, node, minutesLeft)] = maxNeighbour

    return maxNeighbour

score = dfs((), 'AA', 30)
print(score)
