#!/bin/sh

if [ -z ${1} ]; then
  echo "Please provide a day number"
  exit 1
fi

day="day${1}"

echo "New day is ${day}"

mkdir ${day}
touch ${day}/{input.txt,test.txt,main.py}

