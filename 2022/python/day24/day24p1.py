import math
from collections import deque

bliz_chars = "<>^v"

blizzards = tuple(set() for _ in range(4))

for row, line in enumerate(open(0).read().splitlines()[1:]):
    for col, c in enumerate(line[1:]):
        if c in bliz_chars:
            blizzards[bliz_chars.find(c)].add((row, col))

queue = deque([(0, -1, 0)])
seen = set()
destination = (row, col - 1)

repeat_every = row * col // math.gcd(row, col)

while queue:
    time, cr, cc = queue.popleft()

    time += 1

    for dr, dc in [ (-1, 0), (0, -1), (1, 0), (0, 1), (0, 0) ]:
        nr = cr + dr
        nc = cc + dc

        if (nr, nc) == destination:
            print(time)
            exit(0)

        # Hit wall ?
        if (nr < 0 or nc < 0 or nr >= row or nc >= col) and not (nr, nc) == (-1, 0):
            continue

        # Collide with blizzard ?
        for bliz_index, tr, tc in [ (0, 0, -1), (1, 0, 1), (2, -1, 0), (3, 1, 0) ]:
            if ((nr - tr * time) % row, (nc - tc * time) % col) in blizzards[bliz_index]:
                break
        else:
            key = (time % repeat_every, nr, nc)
            if key in seen:
                continue

            seen.add(key)
            queue.append((time, nr, nc))