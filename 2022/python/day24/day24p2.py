import math
from collections import deque

bliz_chars = "<>^v"

blizzards = tuple(set() for _ in range(4))

for row, line in enumerate(open(0).read().splitlines()[1:]):
    for col, c in enumerate(line[1:]):
        if c in bliz_chars:
            blizzards[bliz_chars.find(c)].add((row, col))

queue = deque([ (0, 0, -1, 0) ])
seen = set()
destinations = [ (row, col - 1), (-1, 0) ]

repeat_every = row * col // math.gcd(row, col)

while queue:
    time, stage, cr, cc = queue.popleft()

    time += 1

    for dr, dc in [ (-1, 0), (0, -1), (1, 0), (0, 1), (0, 0) ]:
        nr = cr + dr
        nc = cc + dc

        nstage = stage

        if (nr, nc) == destinations[stage % 2]:
            nstage += 1
            if stage == 2:
                print(time)
                exit(0)

        # Hit wall ?
        if (nr < 0 or nc < 0 or nr >= row or nc >= col) and (nr, nc) not in destinations:
            continue

        # Collide with blizzard ? Do not check on entrance/exit tiles
        collide = False

        if (nr, nc) not in destinations:
            for bliz_index, tr, tc in [ (0, 0, -1), (1, 0, 1), (2, -1, 0), (3, 1, 0) ]:
                if ((nr - tr * time) % row, (nc - tc * time) % col) in blizzards[bliz_index]:
                    collide = True
                    break
        
        if not collide:
            key = (time % repeat_every, nstage, nr, nc)
            if key in seen:
                continue

            seen.add(key)
            queue.append((time, nstage, nr, nc))