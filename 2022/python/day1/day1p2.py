elves = [0]

while True:
    try:
        line = input()
    except:
        break

    if line == "":
        elves.append(0)
        continue

    elves[-1] += int(line)

elves.sort()    

print(sum(elves[-3:]))
