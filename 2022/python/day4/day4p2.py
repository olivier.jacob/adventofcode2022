
total = 0

for line in open(0):
    l1, u1, l2, u2 = map(int, line.replace(',', '-').split('-'))
    if set(range(l1, u1 + 1)) & set(range(l2, u2 + 1)):
        total += 1

print(total)