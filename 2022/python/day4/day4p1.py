
total = 0

for line in open(0):
    l1, u1, l2, u2 = map(int, line.replace(',', '-').split('-'))
    if (l1 <= l2 and u1 >= u2) or (l2 <= l1 and u2 >= u1):
        total += 1

print(total)