

class Element:
    def __init__(self, v):
        self.val = v
        self.next = None
        self.prev = None

DECRYPTION_KEY = 811589153

# Initialize all elements in the list (dll for Double Linked List)
dll = [ Element(int(v) * DECRYPTION_KEY) for v in open(0) ]

# Initialize next / prev for each element in the list
for i in range(len(dll)):
    dll[i].next = dll[(i + 1) % len(dll)]
    dll[i].prev = dll[(i - 1) % len(dll)]

mod = len(dll) - 1

for _ in range(10):
    for elt in dll:
        if elt.val == 0:
            zeroPointer = elt
            continue

        if elt.val > 0:
            # Move forward
            p = elt
            for _ in range(elt.val % mod):
                elt = elt.next
            
            if p == elt:
                continue

            p.next.prev = p.prev
            p.prev.next = p.next
            p.next = elt.next
            p.prev = elt
            elt.next.prev = p
            elt.next = p

        else:
            # Move backward
            p = elt
            for _ in range(-elt.val % mod):
                elt = elt.prev
            
            if p == elt:
                continue

            p.prev.next = p.next
            p.next.prev = p.prev
            p.prev = elt.prev
            p.next = elt
            elt.prev.next = p
            elt.prev = p


total = 0

for _ in range(3):
    for _ in range(1000):
        zeroPointer = zeroPointer.next
    
    total += zeroPointer.val

print(total)