
elves = set()

for row, line in enumerate(open(0)):
    for col, tile in enumerate(line.strip()):
        if tile == '#':
            elves.add(col + row * 1j)

directions = [ -1j, 1j, -1, 1 ]

lookups = {
    -1j: [ -1j -1, -1j, -1j + 1 ],
    -1: [ -1 -1j, -1, -1 + 1j ],
    1j: [ 1j -1, 1j, 1j + 1 ],
    1: [ 1 - 1j, 1, 1 + 1j ]
}

surroundings = [ -1 - 1j, -1j, 1 - 1j, -1, 1, -1 + 1j, 1j, 1 + 1j ]

round = 1
while True:
    once = set()
    twice = set()

    for elf in elves:
        # Elf is surrounded by no other elf -> don't move
        if all(elf + s not in elves for s in surroundings):
            continue
        
        for d in directions:
            if all(elf + l not in elves for l in lookups[d]):
                # Elf can move in that direction
                newPos = elf + d
                
                if newPos in once:
                    twice.add(newPos)
                else:
                    once.add(newPos)

                break

    # All elves had the opportunity to propose a direction, now move them
    currentMap = set(elves)

    for elf in currentMap:
        # Elf is surrounded by no other elf -> don't move
        if all(elf + s not in currentMap for s in surroundings):
            continue

        for d in directions:
            if all(elf + l not in currentMap for l in lookups[d]):
                newPos = elf + d
                if newPos not in twice:
                    elves.remove(elf)
                    elves.add(newPos)
                break

    if not currentMap - elves:
        break

    # First direction becomes last
    directions.append(directions.pop(0)) 

    round += 1



# Compute rectangle containing all elves
minX = min(e.real for e in elves)
minY = min(e.imag for e in elves)

maxX = max(e.real for e in elves)
maxY = max(e.imag for e in elves)

print(round)
