

snafu = "=-012"

total = 0
for line in open(0):
    for p, digit in enumerate(line.strip()[::-1]):
        total += (snafu.find(digit) - 2) * (5 ** p)

snafu_output = ""

while total:
    remainder = total % 5
    total //= 5

    if remainder <= 2:
        snafu_output = str(remainder) + snafu_output
    else:
        snafu_output = "   =-"[remainder] + snafu_output
        total += 1

print(snafu_output)