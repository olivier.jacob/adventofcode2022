import re

nice = 0

for line in open(0).read().splitlines():
    if any([ s in line for s in ["ab", "cd", "pq", "xy"] ]):
        continue

    vowels = re.findall("[aeiou]", line, re.I) 
    if len(vowels) < 3:
        continue

    for c in range(ord('a'), ord('z') + 1):
        t = chr(c) * 2
        if t in line:
            nice += 1
            break
    
print(nice)