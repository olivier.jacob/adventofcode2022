import re

nice = 0

# Letter pairs
pairs = [ chr(c1) + chr(c2) for c1 in range(ord('a'), ord('z') + 1) for c2 in range(ord('a'), ord('z') + 1) ]
# Letters with another letter in between (xyx, efe, aaa)
in_between = [ chr(c1) + chr(c2) + chr(c1) for c1 in range(ord('a'), ord('z') + 1) for c2 in range(ord('a'), ord('z') + 1) ]

for line in open(0).read().splitlines():
    # Contains at least a pair that repeats at least twice
    if all([ len(re.findall(p, line)) < 2 for p in pairs ]):
        continue

    if not any([ s in line for s in in_between ]):
        continue

    nice += 1
    
print(nice)