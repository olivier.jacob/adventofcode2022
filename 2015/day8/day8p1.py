

codeSize = 0
memSize = 0

for line in open(0).read().splitlines():
    s = line.strip()
    memRep = eval("'" + s[1:-1] + "'")

    codeSize += len(s)
    memSize += len(memRep)

print(codeSize - memSize)