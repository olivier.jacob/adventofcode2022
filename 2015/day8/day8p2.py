

originalSize = 0
encodedSize = 0

for line in open(0).read().splitlines():
    s = line.strip()
    originalSize += len(s)

    mem = s[1:-1]
    mem = mem.replace('\\', '\\\\')
    mem = mem.replace('"', "\\\"")

    s = '"\\"' + mem + '"\\"'

    encodedSize += len(s)

print(encodedSize - originalSize)