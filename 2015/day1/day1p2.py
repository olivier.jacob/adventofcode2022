
input = open(0).read()

floor = 0
for i, c in enumerate(input[:]):
    if c == '(':
        floor += 1
    elif c == ')':
        floor -= 1

    if floor == -1:
        print(i + 1)
        exit(0)
