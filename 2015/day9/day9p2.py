import re
from collections import deque

cities = set()
edges = set()

for line in open(0).read().splitlines():
    start, to, distance = re.findall("(\\w+) to (\\w+) = (\\d+)", line)[0]
    
    cities.add(start)
    cities.add(to)

    edges.add((start, to, int(distance)))
    edges.add((to, start, int(distance)))

maxDistance = float('-inf')

for startCity in cities:

    queue = deque([(startCity, 0, [])])

    while queue:
        cur, flewnDistance, alreadyVisited = queue.popleft()

        visited = alreadyVisited[:]
        visited.append(cur)

        allVisited = True
        for s, t, d in edges:
            if s == cur and t not in visited:
                allVisited = False
                queue.append((t, flewnDistance + d, visited))

        if allVisited:
            maxDistance = max(maxDistance, flewnDistance)

print(maxDistance)
