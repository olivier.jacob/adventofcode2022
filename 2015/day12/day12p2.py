import json

def count(js):
    if isinstance(js, dict) and not 'red' in js.values():
        return sum(map(count, js.values()))
    elif isinstance(js, list):
        return sum(map(count, js))
    elif isinstance(js, int):
        return js
    else:
        return 0

print(count(json.loads(open(0).read())))