import re

input = open(0).read().strip()

print(sum(map(int, re.findall("-?\\d+", input))))