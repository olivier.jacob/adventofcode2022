
password = open(0).read().strip()

letters = [ chr(c) for c in range(ord('a'), ord('z') + 1)]

forbidden = [ 'i', 'l', 'o' ]
pairs = [ chr(c) + chr(c) for c in range(ord('a'), ord('z') + 1) ]
straights = [ chr(c) + chr(c + 1) + chr(c + 2) for c in range(ord('a'), ord('x') + 1) ]

def hasNoForbidden(pwd):
    for f in forbidden:
        if f in pwd:
            return False
    return True

def hasStraight(pwd):
    for s in straights:
        if s in pwd:
            return True
    return False

def hasTwoPairs(pwd):
    count = 0
    for p in pairs:
        if p in pwd:
            count += 1
            if count == 2:
                return True
    return False

while True:
    newpwd = list(password)
    for i in range(0, 8)[::-1]:
        ci = letters.index(password[i])

        if (ci + 1) % 26 == 0:
            newpwd[i] = 'a'
        else:
            newpwd[i] = letters[ci + 1]
            break

    password = "".join(newpwd)

    # Validate password
    if hasNoForbidden(password) and hasStraight(password) and hasTwoPairs(password):
        break
        
print(password)