
reindeers = []

for line in open(0).read().splitlines():
    parts = line.strip().split()

    r = parts[0]
    s = int(parts[3])
    ft = int(parts[6])
    rt = int(parts[13])

    # (reindeer name, speed, fly time, rest time, distance so far, points)
    reindeers.append((r, s, ft, rt, 0, 0))

for clock in range(1, 2504):
    newstate = []
    for (r, s, ft, rt, d, p) in reindeers:
        m = clock % (ft + rt)
        if 0 < m <= ft:
            d += s

        newstate.append((r, s, ft, rt, d, p))

    m = max(map(lambda r: r[4], newstate))

    reindeers = []
    for (r, s, ft, rt, d, p) in newstate:
        if d == m:
            reindeers.append((r, s, ft, rt, d, p + 1))
        else:
            reindeers.append((r, s, ft, rt, d, p))
        
print(max(map(lambda r: r[5], reindeers)))