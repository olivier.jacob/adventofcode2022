from itertools import groupby

ROUNDS = 50

sequence = open(0).read()

for _ in range(ROUNDS):
    sequence = "".join([ str(len(list(sc))) + c for c, sc in groupby(sequence) ])

print(len(sequence))