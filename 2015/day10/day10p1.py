
ROUNDS = 40

sequence = open(0).read()

for _ in range(ROUNDS):
    c = sequence[0]
    sc = 1
    lookandsay = ""

    for cc in sequence[1:]:
        if c == cc:
            sc += 1
        else:
            # Say
            lookandsay += str(sc)
            lookandsay += c
            
            # Prepare next character
            c = cc
            sc = 1

    lookandsay += str(sc)
    lookandsay += c

    sequence = lookandsay

print(len(sequence))