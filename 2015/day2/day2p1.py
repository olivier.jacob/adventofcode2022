

# 2*l*w + 2*w*h + 2*h*l.

total = 0

for gift in open(0).read().splitlines():
    l, w, h = map(int, gift.split('x'))
    total += 2*l*w + 2*w*h + 2*h*l + min([l*w, l*h, w*h])

print(total)