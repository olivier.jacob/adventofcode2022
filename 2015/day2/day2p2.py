

# 2*l*w + 2*w*h + 2*h*l.

total = 0

for gift in open(0).read().splitlines():
    l, w, h = map(int, gift.split('x'))

    dim = sorted([l, w, h])

    ribbon = dim[0] * 2 + dim[1] * 2
    bow = l*w*h

    total += ribbon + bow

print(total)