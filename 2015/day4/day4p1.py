import hashlib

input = open(0).read()

key = 1
while True:
    istr = input + str(key)
    if hashlib.md5(istr.encode()).hexdigest().startswith("00000"):
        print(key)
        exit(0)
    key += 1
