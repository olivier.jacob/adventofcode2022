

instructions = open(0).read()

# 0 is Santa, 1 is RoboSanta
current = [0, 0]

visited = set()
visited.add(0) # The starting house is always visited

for turn, instr in enumerate(instructions[:]):
    if instr == '^':
        current[turn % 2] = current[turn % 2] - 1j
    elif instr == '>':
        current[turn % 2] = current[turn % 2] + 1
    elif instr == '<':
        current[turn % 2] = current[turn % 2] - 1
    elif instr == 'v':
        current[turn % 2] = current[turn % 2] + 1j

    visited.add(current[turn % 2])

print(len(visited))    