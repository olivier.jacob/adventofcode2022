

instructions = open(0).read()

current = 0
visited = set()
visited.add(current) # The starting house is always visited

for instr in instructions[:]:
    if instr == '^':
        current -= 1j
    elif instr == '>':
        current += 1
    elif instr == '<':
        current -= 1
    elif instr == 'v':
        current += 1j

    visited.add(current)

print(len(visited))    