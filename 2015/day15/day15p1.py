import re

ingredients = []

for line in open(0).read().splitlines():
    parts = line.split()

    cap, d, f, t, cal = map(int, re.findall('-?\d+', line))
    ingredients.append((cap, d, f, t, cal))

m = 0

for i in range(0, 100):
    for j in range(0, 100 - i):
        for k in range(0 ,100 - i - j):
            l = 100 - i - j - k

            cap = max(0, sum([ ingredients[0][0] * i + ingredients[1][0] * j + ingredients[2][0] * k + ingredients[3][0] * l ]))
            dur = max(0, sum([ ingredients[0][1] * i + ingredients[1][1] * j + ingredients[2][1] * k + ingredients[3][1] * l ]))
            flav = max(0, sum([ ingredients[0][2] * i + ingredients[1][2] * j + ingredients[2][2] * k + ingredients[3][2] * l ]))
            text = max(0, sum([ ingredients[0][3] * i + ingredients[1][3] * j + ingredients[2][3] * k + ingredients[3][3] * l ]))

            m = max(m, cap * dur * flav * text)

print(m)
