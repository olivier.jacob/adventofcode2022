import re

grid = []

for r in range(1000):
    grid.append([])
    for c in range(1000):
        grid[r].append(0)

for line in open(0).read().splitlines():
    cmd, fromX, fromY, toX, toY = re.findall("(turn on|turn off|toggle) (\\d+),(\\d+) through (\\d+),(\\d+)", line)[0]

    fromX, fromY, toX, toY = map(int, [fromX, fromY, toX, toY])

    for r in range(fromX, toX + 1):
        for c in range(fromY, toY + 1):
            if cmd == "turn on":
                grid[r][c] = grid[r][c] + 1
            elif cmd == "turn off":
                grid[r][c] = max(0, grid[r][c] - 1)
            elif cmd == "toggle":
                grid[r][c] = grid[r][c] + 2


onCount = 0
for r in range(1000):
    for c in range(1000):
            onCount += grid[r][c]

print(onCount)
