import re

grid = []

for r in range(1000):
    grid.append([])
    for c in range(1000):
        grid[r].append(False)

for line in open(0).read().splitlines():
    cmd, fromX, fromY, toX, toY = re.findall("(turn on|turn off|toggle) (\\d+),(\\d+) through (\\d+),(\\d+)", line)[0]

    fromX, fromY, toX, toY = map(int, [fromX, fromY, toX, toY])

    for r in range(fromX, toX + 1):
        for c in range(fromY, toY + 1):
            if cmd == "turn on":
                grid[r][c] = True
            elif cmd == "turn off":
                grid[r][c] = False
            elif cmd == "toggle":
                grid[r][c] = not grid[r][c]


onCount = 0
for r in range(1000):
    for c in range(1000):
        if grid[r][c]:
            onCount += 1

print(onCount)
