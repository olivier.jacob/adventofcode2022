import re
from collections import deque

wires = {}
instrs = deque(open(0).read().splitlines())

while instrs:

    instr = instrs.popleft()

    if re.match("(\\w+) -> (\\w+)", instr):
        val, wire = re.findall("(\\w+) -> (\\w+)", instr)[0]

        if val.isnumeric():
            wires[wire] = int(val)
        else:
            if val not in wires:
                instrs.append(instr)
                continue

            wires[wire] = wires[val]

    elif re.match("NOT (\\w+) -> (\\w+)", instr):
        wire, targetwire = re.findall("NOT (\\w+) -> (\\w+)", instr)[0]

        if wire not in wires:
            instrs.append(instr)
            continue

        wires[targetwire] = ~wires[wire] % 65536

    elif re.match("(\\d+) (AND|OR) (\\w+) -> (\\w+)", instr):
        val, op, wire2, targetwire = re.findall("(\\w+) (AND|OR) (\\w+) -> (\\w+)", instr)[0]

        if wire2 not in wires:
            instrs.append(instr)
            continue

        if op == "AND":
            wires[targetwire] = int(val) & wires[wire2]
        elif op == "OR":
            wires[targetwire] = int(val) | wires[wire2]

    elif re.match("(\\w+) (AND|OR) (\\w+) -> (\\w+)", instr):
        wire1, op, wire2, targetwire = re.findall("(\\w+) (AND|OR) (\\w+) -> (\\w+)", instr)[0]

        if wire1 not in wires or wire2 not in wires:
            instrs.append(instr)
            continue

        if op == "AND":
            wires[targetwire] = wires[wire1] & wires[wire2]
        elif op == "OR":
            wires[targetwire] = wires[wire1] | wires[wire2]

    elif re.match("(\\w+) (LSHIFT|RSHIFT) (\\d+) -> (\\w+)", instr):
        wire, op, shift, targetwire = re.findall("(\\w+) (LSHIFT|RSHIFT) (\\d+) -> (\\w+)", instr)[0]

        if wire not in wires:
            instrs.append(instr)
            continue

        if op == "LSHIFT":
            wires[targetwire] = wires[wire] << int(shift)
        elif op == "RSHIFT":
            wires[targetwire] = wires[wire] >> int(shift)

print(wires['a'])