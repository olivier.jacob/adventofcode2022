import re
from collections import deque

guests = {}
changes = {}

for line in open(0).read().splitlines():
    name, op, units, neighbour = re.findall("(\w+) would (gain|lose) (\d+) happiness units by sitting next to (\w+).", line)[0]

    if name not in guests:
        guests[name] = []

    neighbours = guests[name]

    dUnits = 0
    if op == 'gain':
        dUnits = int(units)
    else:
        dUnits = -int(units)

    if (neighbour, name) in changes:
        changes[(neighbour, name)] += dUnits
        changes[(name, neighbour)] = changes[(neighbour, name)]
    else:
        changes[(name, neighbour)] = dUnits

    neighbours.append(neighbour)
    guests[name] = neighbours

maxChange = 0

for g in guests.keys():

    queue = deque([(g, [])])
    
    while queue:
        cg, visited = queue.popleft()

        alreadyVisited = visited[:]
        alreadyVisited.append(cg)

        enqueued = False
        for n in guests[cg]:
            if n not in visited:
                queue.append((n, alreadyVisited))
                enqueued = True

        if not enqueued:
            totalChange = 0
            for i in range(len(alreadyVisited)):
                for k in guests.keys():
                    if k == alreadyVisited[(i + 1) % len(alreadyVisited)]:
                        rightNeighbour = k
                totalChange += changes[(alreadyVisited[i], rightNeighbour)]

            maxChange = max(maxChange, totalChange)

print(maxChange) 
